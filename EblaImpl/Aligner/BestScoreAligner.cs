﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */
using EblaAPI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EblaImpl.Aligner
{
    public class BestScoreAligner
    {
        private const float InitialFraction = 0.85f;

        public List<BestAlignment> SelectBestScoreAlignments(List<BestAlignment> alignmentList)
        {
            var bestOneToOneAlignments = SelectOneToOneBestAlignments(alignmentList);
            var bestAlignments = SelectFractionBestAlignments(bestOneToOneAlignments);

            return bestAlignments;
        }

        private List<BestAlignment> SelectFractionBestAlignments(List<BestAlignment> alignmentList)
        {
            var threshold = CalculateThreshold(alignmentList);
            return alignmentList.Where(alignment => alignment.Score <= threshold).ToList();
        }

        private List<BestAlignment> SelectOneToOneBestAlignments(List<BestAlignment> alignmentList)
        {
            var bestAlignments =
                alignmentList.Where(
                        alignment => alignment.BaseSegmentList.Length == 1 && alignment.VersionSegmentList.Length == 1)
                    .ToList();
            return bestAlignments;
        }

        private static float CalculateThreshold(List<BestAlignment> alignmentList)
        {
            var scoreArray = new float[alignmentList.Count];
            var index = 0;
            foreach (var alignment in alignmentList)
            {
                scoreArray[index] = alignment.Score;
                ++index;
            }
            Array.Sort(scoreArray);
            var firstFiltered = InitialFraction * scoreArray.Length - 0.5f;
            var threshold = firstFiltered < 0.0f ? float.NegativeInfinity : scoreArray[(int)firstFiltered];

            return threshold;
        }
    }
}
