﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using EblaAPI;
using EblaImpl.Algorithm;
using EblaImpl.Calculator;
using EblaImpl.Translation;

namespace EblaImpl.Aligner
{
    public class TranslationAligner : OpenableEblaItem, ITranslationAligner
    {
        private const string TotalTokensAttributeName = ":totaltokens";
        private const string TokensAttributeName = ":tokens";
        private const string WidsAttributeName = ":wids";
        private SingletonDb _conn = SingletonDb.Instance;

        public TranslationAligner(NameValueCollection configProvider) : base(configProvider)
        {
        }

        private void CheckConnectionStatus()
        {
            _conn = SingletonDb.Instance;
            _conn.GetConnection();

            _open = true;
        }

        public TranslationDocument GetMooreTranslationDocument(string corpusName, string versionName, string username,
            string password)
        {
            CheckConnectionStatus();

            var corpus = new Corpus(ConfigProvider);
            corpus.OpenWithoutPrivilegeCheck(corpusName, true, GetConnection());

            var translationDocument = new TranslationDocument();
            translationDocument.CorpusName = corpusName;
            translationDocument.VersionName = versionName;

            var translationUtil = new TranslationUtil();

            translationDocument.BaseDocument = (Document) EblaFactory.GetDocument(ConfigProvider);
            if (!translationDocument.BaseDocument.OpenBaseText(corpusName, username, password))
                throw new Exception("You do not have permission to access the document.");

            var sentenceAlignment = new SegmentBasedAlignment(ConfigProvider);
            translationDocument.BaseDocumentSegmentsTotalTokens =
                sentenceAlignment.GetSegmentTokens(((Document) translationDocument.BaseDocument).ID(),
                    TotalTokensAttributeName);

            translationDocument.VersionDocument = (Document) EblaFactory.GetDocument(ConfigProvider);
            if (!translationDocument.VersionDocument.OpenVersion(corpusName, versionName, username, password))
                throw new Exception("You do not have permission to access the document.");

            translationDocument.VersionDocumentSegmentsTotalTokens =
                sentenceAlignment.GetSegmentTokens(((Document) translationDocument.VersionDocument).ID(),
                    TotalTokensAttributeName);

            translationDocument.BaseDocumentSegmentsWids =
                sentenceAlignment.GetSegmentTokens(((Document) translationDocument.BaseDocument).ID(),
                    WidsAttributeName);

            translationDocument.VersionDocumentSegmentsWids =
                sentenceAlignment.GetSegmentTokens(((Document) translationDocument.VersionDocument).ID(),
                    WidsAttributeName);

            translationDocument.BaseDocumentSegmentsTokens =
                sentenceAlignment.GetSegmentTokens(((Document) translationDocument.BaseDocument).ID(),
                    TokensAttributeName);

            translationDocument.BaseVocabulary =
                translationUtil.CreateAlignmentVocabulary(translationDocument.BaseDocumentSegmentsTokens);

            translationDocument.VersionDocumentSegmentsTokens =
                sentenceAlignment.GetSegmentTokens(((Document) translationDocument.VersionDocument).ID(),
                    TokensAttributeName);

            translationDocument.VersionVocabulary =
                translationUtil.CreateAlignmentVocabulary(translationDocument.VersionDocumentSegmentsTokens);

            return translationDocument;
        }

        public List<BestAlignment> ApplyMooreAlignment(string corpusName, string versionName, string username,
            string password)
        {
            var mooreTranslationDocument = GetMooreTranslationDocument(corpusName, versionName, username, password);

            var segmentLengthAlignments = ApplyMooreAlignmentFirstStage(mooreTranslationDocument);

            var bestAlignments = ApplyMooreBestAlignments(segmentLengthAlignments);

            var contentAlignments = ApplyMooreAlignmentSecondStage(mooreTranslationDocument, bestAlignments);

            var unifiedAlignments = ApplyMooreAlignmentThirdStage(mooreTranslationDocument, contentAlignments);

            return unifiedAlignments;
        }

        public bool ApplyMooreAlignment(TranslationDocument translationDocument, IAlignmentSet alignmentSet, out int progress)
        {
            CheckConnectionStatus();

            var segmentLengthAlignments = ApplyMooreAlignmentFirstStage(translationDocument);
            var bestAlignments = ApplyMooreBestAlignments(segmentLengthAlignments);
            var contentAlignments = ApplyMooreAlignmentSecondStage(translationDocument, bestAlignments);
            var unifiedAlignments = ApplyMooreAlignmentThirdStage(translationDocument, contentAlignments);

            var isMoreToDo = ((AlignmentSet)alignmentSet).CreateMooreAlignments(translationDocument, unifiedAlignments, out progress);

            return isMoreToDo;
        }

        private List<BestAlignment> ApplyMooreBestAlignments(List<BestAlignment> segmentLengthAlignments)
        {
            CheckConnectionStatus();
            var bestScoreAligner = new BestScoreAligner();
            var bestAlignments = bestScoreAligner.SelectBestScoreAlignments(segmentLengthAlignments);

            return bestAlignments;
        }

        private List<BestAlignment> ApplyMooreAlignmentFirstStage(TranslationDocument translationDocument)
        {
            CheckConnectionStatus();

            var fwdbwd = new ForwardBackwardAlgorithm
            {
                Calculator =
                    new SegmentCalculator(translationDocument.BaseDocumentSegmentsTotalTokens,
                        translationDocument.VersionDocumentSegmentsTotalTokens)
            };

            var fwdbwdAlignments = fwdbwd.ForwardBackward(translationDocument.BaseDocumentSegmentsTotalTokens,
                translationDocument.VersionDocumentSegmentsTotalTokens);

            return fwdbwdAlignments;
        }

        private List<BestAlignment> ApplyMooreAlignmentSecondStage(TranslationDocument translationDocument,
            List<BestAlignment> bestAlignments)
        {
            CheckConnectionStatus();

            var ibmAligner = new IbmModelAligner();

            var translationData = ibmAligner.GetTranslationData(translationDocument, bestAlignments);

            var fwdbwd = new ForwardBackwardAlgorithm
            {
                Calculator =
                    new WordCalculator(translationDocument)
                    {
                        WordTranslationData = translationData
                    }
            };
            var bestTrainedAlignments = fwdbwd.ForwardBackward(translationDocument.BaseDocumentSegmentsWids,
                translationDocument.VersionDocumentSegmentsWids);

            return bestTrainedAlignments;
        }

        private List<BestAlignment> ApplyMooreAlignmentThirdStage(TranslationDocument translationDocument,
            List<BestAlignment> secondStageAlignments)
        {
            CheckConnectionStatus();

            var unify = new UnifyAligner();
            var unifiedAlignments = unify.UnifyAlignments(translationDocument, secondStageAlignments);

            return unifiedAlignments;
        }

        public bool ContinueAutoAlignment(TranslationDocument translationDocument, IAlignmentSet alignmentSet, out int progress)
        {
            CheckConnectionStatus();
            var isMoreToDo = ApplyMooreAlignment(translationDocument, alignmentSet, out progress);

            return isMoreToDo;
        }

    }
}