﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml;
using EblaAPI;
using System.Security.Cryptography;
using System.Web.Security;

#if _USE_MYSQL
using MySql.Data.MySqlClient;
using EblaDataReader = MySql.Data.MySqlClient.MySqlDataReader;
using EblaConnection = MySql.Data.MySqlClient.MySqlConnection;
using EblaCommand = MySql.Data.MySqlClient.MySqlCommand;
using EblaParameter = MySql.Data.MySqlClient.MySqlParameter;
using EblaTransaction = MySql.Data.MySqlClient.MySqlTransaction;
#else
using System.Data.SQLite;
using EblaDataReader = System.Data.SQLite.SQLiteDataReader;
using EblaConnection = System.Data.SQLite.SQLiteConnection;
using EblaCommand = System.Data.SQLite.SQLiteCommand;
using EblaParameter = System.Data.SQLite.SQLiteParameter;
using EblaTransaction = System.Data.SQLite.SQLiteTransaction;
#endif

namespace EblaImpl
{
    public class EblaHelpers
    {
        static HashSet<string> _inlineTags = new HashSet<string>() { "a", "abbr", "acronym", "b", "basefont", "bdo", "big", 
                                "br", "cite", "code", "dfn", "em", "font", "i",
                                "img", "input", "kbd", "label", "q", "s", "samp", "select", "small",
                                "span", "strike", "strong", "sub", "sup", "textarea", "tt", "u", "var", 
                                "ins", "del", "applet", "button", "iframe", "map", "object", "script"}; // Strictly speaking, these can be used as block-level and 
        // contain other block-level elements. See comments within the code that uses 
        // this HashSet.


        public static bool IsInlineTag(string tagName)
        {
            return _inlineTags.Contains(tagName.ToLower());
        }

        static internal int GetLastInsertID(EblaCommand cmd)
        {
#if _USE_MYSQL
                  cmd.CommandText = "select last_insert_id()";
#else
            cmd.CommandText = "select last_insert_rowid()";
#endif

            using (EblaDataReader dr = cmd.ExecuteReader())
            {
                if (!dr.Read())
                    throw new Exception("Unable to obtain last_insert_id");

                return dr.GetInt32(0);

            }

        }
#region "WCF support"

        public static string CorpusStoreBaseAddressHttp(string server)
        {
            return "http://" + server + ":8080/corpusstore";
        }

        public static string CorpusStoreBaseAddressNetTcp(string server)
        {
            return "net.tcp://" + server + ":28000/corpusstore";
        }

#endregion


        internal static string GetConnStr(System.Collections.Specialized.NameValueCollection configProvider)
        {
            return configProvider["ConnStr"];

        }

        internal static EblaConnection GetConnection(System.Collections.Specialized.NameValueCollection configProvider)
        {
            EblaConnection cn = new EblaConnection(GetConnStr(configProvider));

            cn.Open();

            return cn;
        }

        internal static bool Authenticate(string Username, string Password, EblaConnection cn, out bool isAdmin)
        {
            isAdmin = false;


            using (EblaCommand cmd = new EblaCommand("SELECT Password, Salt, IsAdmin FROM users WHERE Username = @username", cn))
            {
                cmd.Parameters.AddWithValue("username", Username);

                using (EblaDataReader dr = cmd.ExecuteReader())
                {

                    if (!dr.Read())
                        return false;

                    string pwd = dr.GetString(dr.GetOrdinal("Password"));
                    int saltOrdinal = dr.GetOrdinal("Salt");
                    if (dr.IsDBNull(saltOrdinal))
                        return false;

                    string salt = dr.GetString(saltOrdinal);
                    string hashedpassword = EblaHelpers.CreateHashedPassword(salt, Password);

                    if (string.Compare(pwd, hashedpassword) != 0)
                        return false;

                    isAdmin = dr.GetBoolean(dr.GetOrdinal("IsAdmin"));

                }

            }

            return true;

        }

        internal static string CreateSalt()
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

            byte[] buffer = new byte[4];
            rng.GetBytes(buffer);

            return Convert.ToBase64String(buffer);
        }

        internal static string CreateHashedPassword(string salt, string password)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(string.Concat(salt, password), "SHA1");
        }

        internal static string GeneratePassword()
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

            byte[] buffer = new byte[64];
            rng.GetBytes(buffer);

            int seed = 0;
            for (int i = 0; i < buffer.Length; i++)
                seed += buffer[i];

            Random rnd = new Random(seed);

            string characters = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789";

            char[] password = new char[8];

            for (int i = 0; i < 8; i++)
                password[i] = characters[rnd.Next(0, characters.Length)];

            return new string(password);
        }


        internal static bool IsCorpusNameValid(string name, bool throwEx)
        {
            // For now, anything non-empty
            if (string.IsNullOrWhiteSpace(name))
            {
                if (throwEx)
                    throw new Exception("Corpus name not valid: " + name);
                return false;
            }
            return true;
        }

        internal static bool IsVersionNameValid(string name, bool throwEx)
        {
            // For now, anything non-empty
            if (string.IsNullOrWhiteSpace(name))
            {
                if (throwEx)
                    throw new Exception("Version name not valid: " + name);
                return false;
            }
            return true;
        }

        public static XmlDocument CreateXmlDocument(string rootElmName)
        {
            XmlDocument xmlDoc = new XmlDocument();
#if _VVV_PRESERVEWHITESPACE
            xmlDoc.PreserveWhitespace = true;
#endif            
            XmlDeclaration dec = xmlDoc.CreateXmlDeclaration("1.0", null, null);
            xmlDoc.AppendChild(dec);// Create the root element
            XmlElement root = xmlDoc.CreateElement(rootElmName);
            xmlDoc.AppendChild(root);
            return xmlDoc;
        }

        public static void ConvertWhitespaceNodesToTextNodes(XmlNode node)
        {
            if (node.NodeType == XmlNodeType.Whitespace)
                throw new Exception("ConvertWhitespaceNodesToTextNodes called on whitespace node.");

            List<XmlNode> nodesToConvert = new List<XmlNode>();
            //List<XmlNode> prevSiblings = new List<XmlNode>();
            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.NodeType)
                {
                    case XmlNodeType.Whitespace:
                    case XmlNodeType.SignificantWhitespace:
                        nodesToConvert.Add(child);
                        //prevSiblings.Add(child.PreviousSibling);
                        break;
                    default:
                        ConvertWhitespaceNodesToTextNodes(child);
                        break;
                }
            }

            if (nodesToConvert.Count > 0)
            {
                for (int i = 0; i < nodesToConvert.Count; i++)
                {
                    XmlText t = node.OwnerDocument.CreateTextNode(nodesToConvert[i].InnerText);
                    node.ReplaceChild(t, nodesToConvert[i]);

                }
            }

        }

        internal static int GetNodeId(XmlNode node)
        {
            if (node.Attributes == null)
                throw new Exception("Attributes null in GetNodeId");
            if (node.Attributes.Count == 0)
                throw new Exception("Attributes empty in GetNodeId");

            XmlAttribute attrib = node.Attributes["id"];
            if (attrib == null)
                throw new Exception("Missing id attribute in GetNodeId");


            Int32 id = -1;
            if (Int32.TryParse(attrib.Value, out id))
                return id;

            throw new Exception("Invalid id in GetNodeId: " + attrib.Value);
        }

        public static string XmlDocumentToString(XmlDocument doc)
        {
            System.IO.StringWriter sw = new System.IO.StringWriter();

            XmlTextWriter tw = new XmlTextWriter(sw);
            tw.Formatting = Formatting.None;
           
#if _VVV_PRESERVEWHITESPACE
            doc.PreserveWhitespace = true;
#else
            doc.PreserveWhitespace = false;
#endif
            doc.WriteContentTo(tw);
            tw.Close();

            return sw.ToString();

        }

        ///Calculates the value of a Poisson distribution function at given point (e). 
        ///mean is the number of times an event occurs in an interval and e can take values 0, 1, 2, …
        public static float PoissonDistribution(float mean, int e)
        {
            var result = mean + -e * (float)Math.Log(mean) + Factorial(e);
            return result;
        }

        /// Calculates the logarithm from factorial of a given number ln(x!).
        public static float Factorial(int k)
        {
            if (k >= 0)
            {
                float factorial = 0;
                for (var count = 2; count <= k; count++)
                {
                    factorial += (float)Math.Log(count);
                }
                return factorial;
            }
            else
            {
                throw new Exception("Cannot calculate factorial for a negative number: " + k + ".");
            }
        }

        public static string GetEnumName(Enum value)
        {
            return Enum.GetName(value.GetType(), value);
        }

        public static string GetEnumDescription(Enum value)
        {
            var fieldInfo = value.GetType().GetField(GetEnumName(value));
            var descriptionAttribute = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault() as DescriptionAttribute;
            return descriptionAttribute == null ? GetEnumName(value) : descriptionAttribute.Description;
        }
    }

    public class XmlDocPos
    {
        public XmlText textContent;
        public int offset;

        private class MeasureStatus
        {
            public MeasureStatus() { EndFound = false; }
            public bool EndFound;
        }


        public delegate bool IgnoreElmDelg(XmlNode node);

        public static XmlDocPos SeekToOffsetEx(int targetOffset, XmlNode node, IgnoreElmDelg IgnoreElmImpl, ref XmlNode lastNonTextNode, ref int lastNonTextNodeOffset)
        {
            int offset = 0;

            if (lastNonTextNode != null)
            {
                if (lastNonTextNodeOffset >= targetOffset)
                {
                    //System.Diagnostics.Debug.WriteLine("SeekToOffsetEx, hint value was no use");

                }
                else
                {
                    offset += lastNonTextNodeOffset;
                    node = lastNonTextNode;

                }
            }


            XmlDocPos pos = DoSeekToOffsetEx(targetOffset, ref offset, node, IgnoreElmImpl, ref lastNonTextNode, ref lastNonTextNodeOffset);
            if (pos != null)
                return pos;

            // So - that looked at node and all its siblings (and their children)
            // Try looking from parent's sibling onwards
            node = node.ParentNode;
            do
            {
                // Find next sibling to examine (parent's sibling, or grandparent's sibling, etc.)
                while (node != null)
                {
                    if (node.NextSibling != null)
                    {
                        node = node.NextSibling;
                        break;
                    }
                    node = node.ParentNode;

                }

                if (node == null)
                    break;

                pos = DoSeekToOffsetEx(targetOffset, ref offset, node, IgnoreElmImpl, ref lastNonTextNode, ref lastNonTextNodeOffset);
                if (pos != null)
                    return pos;

                node = node.ParentNode;

            } while (true);


            return null;
        }

        private static XmlDocPos DoSeekToOffsetEx(int targetOffset, ref int offset, XmlNode node, IgnoreElmDelg IgnoreElmImpl, ref XmlNode lastNonTextNode, ref int lastNonTextNodeOffset)
        {
            while (node != null)
            {
                if (IgnoreElmImpl.Invoke(node))
                {
                    node = node.NextSibling;
                    continue;
                }

                if (node.NodeType == XmlNodeType.Text)
                {
                    XmlText textContent = (XmlText)node;

                    if (textContent.Length + offset >= targetOffset)
                    {
                        XmlDocPos pos = new XmlDocPos();
                        pos.textContent = textContent;
                        pos.offset = targetOffset - offset;
                        return pos;
                    }
                    offset += textContent.Length;

                }
                else
                {
                    lastNonTextNode = node;
                    lastNonTextNodeOffset = offset;
                }

                if (node.ChildNodes.Count > 0)
                {
                    XmlNode child = node.FirstChild;
                    XmlDocPos pos = DoSeekToOffsetEx(targetOffset, ref offset, child, IgnoreElmImpl, ref lastNonTextNode, ref lastNonTextNodeOffset); // elmsToIgnore);
                    if (pos != null)
                        return pos;

                }


                node = node.NextSibling;
            }

            //System.Diagnostics.Debug.Assert(false);
            return null;

        }

        public static XmlDocPos Find(string text, XmlNode node, IgnoreElmDelg IgnoreElmImpl)
        {

            if (node.NodeType == XmlNodeType.Text)
            {
                XmlText textContent = (XmlText)node;

                int offset = textContent.Value.IndexOf(text);
                if (offset > -1)
                {
                    XmlDocPos pos = new XmlDocPos();
                    pos.textContent = textContent;
                    pos.offset = offset;
                    return pos;
                }
            }

            foreach (XmlNode child in node.ChildNodes)
            {
                if (IgnoreElmImpl.Invoke(child)) //  elmsToIgnore.Contains(child))
                {
                    // Skip this - e.g. it's a segment marker element we've inserted dynamically
                    
                }
                else
                {
                    XmlDocPos pos = Find(text, child, IgnoreElmImpl);
                    if (pos != null)
                        return pos;

                }

            }

            //System.Diagnostics.Debug.Assert(false);
            return null;

        }

        public static XmlDocPos SeekToOffset(ref int offset, XmlNode node, IgnoreElmDelg IgnoreElmImpl)//  List<XmlElement> elmsToIgnore)
        {

            if (IgnoreElmImpl.Invoke(node))
                throw new Exception("SeekToOffset - node must not be in elmsToIgore");

            if (node.NodeType == XmlNodeType.Text)
            {
                XmlText textContent = (XmlText)node;

                if (textContent.Length >= offset)
                {
                    XmlDocPos pos = new XmlDocPos();
                    pos.textContent = textContent;
                    pos.offset = offset;
                    return pos;
                }
                offset -= textContent.Length;

            }

            foreach (XmlNode child in node.ChildNodes)
            {
                if (IgnoreElmImpl.Invoke(child)) //  elmsToIgnore.Contains(child))
                {
                    // Skip this - e.g. it's a segment marker element we've inserted dynamically
                    
                }
                else
                {
                    XmlDocPos pos = SeekToOffset(ref offset, child, IgnoreElmImpl); // elmsToIgnore);
                    if (pos != null)
                        return pos;

                }

            }

            //System.Diagnostics.Debug.Assert(false);
            return null;
        }

        public static int MeasureBetween(XmlDocPos startPos, XmlDocPos endPos, bool expectWhitespace, IgnoreElmDelg IgnoreElmImpl)
        {
            if (startPos == null || endPos == null)
                throw new ArgumentNullException();
           
            System.Diagnostics.Debug.Assert(object.ReferenceEquals(startPos.textContent.OwnerDocument, endPos.textContent.OwnerDocument));

            return MeasureTo(endPos, endPos.textContent.OwnerDocument.DocumentElement, expectWhitespace, new MeasureStatus(), IgnoreElmImpl) - MeasureTo(startPos, startPos.textContent.OwnerDocument.DocumentElement, expectWhitespace, new MeasureStatus(), IgnoreElmImpl);
        }

        public static int MeasureTo(XmlDocPos pos, bool expectWhitespace, IgnoreElmDelg IgnoreElmImpl)
        {
            return MeasureTo(pos, pos.textContent.OwnerDocument.DocumentElement, expectWhitespace, new MeasureStatus(), IgnoreElmImpl);
        }

        public static void Extract(XmlDocPos pos, XmlNode currNode, int Length, bool expectWhitespace, IgnoreElmDelg IgnoreElmImpl, List<string> collator, ref int count)
        {
            bool firstCall = false;
            if (currNode == null)
            {
                currNode = pos.textContent;
                firstCall = true;
            }

            bool ignore = false;
            switch (currNode.NodeType)
            {
                case XmlNodeType.Element:
                    if (IgnoreElmImpl != null)
                        ignore = IgnoreElmImpl.Invoke(currNode);
                    break;
                case XmlNodeType.Text:
                    if (firstCall)
                    {
                        if (currNode.Value.Length - pos.offset >= Length)
                        {
                            collator.Add(currNode.Value.Substring(pos.offset, Length));
                            count = Length;
                            pos.offset = pos.offset + Length;
                            return;
                        }
                        collator.Add(currNode.Value.Substring(pos.offset));
                        count += currNode.Value.Length - pos.offset;
                    }
                    else
                    {
                        if (count + currNode.Value.Length >= Length)
                        {
                            collator.Add(currNode.Value.Substring(0, Length - count));
                            pos.offset = Length - count;
                            pos.textContent = (XmlText)currNode;
                            count = Length;
                            return;
                        }

                        collator.Add(currNode.Value);
                        count += currNode.Value.Length;
                    }
                    break;
                case XmlNodeType.Whitespace:
                case XmlNodeType.SignificantWhitespace:
                    if (expectWhitespace)
                    {
                        count += currNode.Value.Length;
                        break;
                    }

                    goto default;
                default:
                    throw new Exception("Unexpected XmlNodeType in XmlDocPos.MeasureTo: " + currNode.NodeType.ToString());
            }


            if (!ignore)
                foreach (XmlNode child in currNode.ChildNodes)
                {
                    Extract(pos, child, Length, expectWhitespace, IgnoreElmImpl, collator, ref count);
                    //count += MeasureTo(endPos, child, expectWhitespace, status, IgnoreElmImpl);
                    System.Diagnostics.Debug.Assert(count <= Length);
                    if (count == Length)
                        return;

                }
            //return count;

            while (currNode.NextSibling != null)
            {
                currNode = currNode.NextSibling;
                Extract(pos, currNode, Length, expectWhitespace, IgnoreElmImpl, collator, ref count);
                if (count == Length)
                    return;
            }

            currNode = currNode.ParentNode;
                // climb up ancestors until a next sibling is found.
            while (currNode != null)
            {
                if (currNode.NextSibling != null)
                {
                    currNode = currNode.NextSibling;
                    break;
                }
                currNode = currNode.ParentNode;
            }
            if (currNode != null)
                Extract(pos, currNode, Length, expectWhitespace, IgnoreElmImpl, collator, ref count);

            System.Diagnostics.Debug.Assert(count == Length);            

        }

        private static int MeasureTo(XmlDocPos pos, XmlNode currNode, bool expectWhitespace, MeasureStatus status, IgnoreElmDelg IgnoreElmImpl)
        {
            //System.Diagnostics.Debug.Assert(object.ReferenceEquals(pos.textContent.OwnerDocument, currNode.OwnerDocument));

            if (object.ReferenceEquals(currNode, pos.textContent))
            {
                status.EndFound = true;
                return pos.offset;
            }

            int count = 0;

            bool ignore = false;
            switch (currNode.NodeType)
            {
                case XmlNodeType.Element:
                    if (IgnoreElmImpl != null)
                        ignore = IgnoreElmImpl.Invoke(currNode);
                    break;
                case XmlNodeType.Text:
                    count += currNode.Value.Length;
                    break;
                case XmlNodeType.Whitespace:
                case XmlNodeType.SignificantWhitespace:
                    if (expectWhitespace)
                    {
                        count += currNode.Value.Length;
                        break;
                    }

                    goto default;
                default:
                    throw new Exception("Unexpected XmlNodeType in XmlDocPos.MeasureTo: " + currNode.NodeType.ToString());
            }


            if (!ignore)
                foreach (XmlNode child in currNode.ChildNodes)
                {
                    count += MeasureTo(pos, child, expectWhitespace, status, IgnoreElmImpl);
                    if (status.EndFound)
                        break;

                }
            return count;
        
        }

        public static int Measure(XmlNode node, bool expectWhitespace)
        {
            int count = 0;

            switch (node.NodeType)
            {
                case XmlNodeType.Element:
                    break;
                case XmlNodeType.Text:
                    count += node.Value.Length;
                    break;
                case XmlNodeType.Whitespace:
                case XmlNodeType.SignificantWhitespace:
                    if (expectWhitespace)
                    {
                        count += node.Value.Length;
                        break;
                    }
                     
                    goto default;
                default:
                    throw new Exception("Unexpected XmlNodeType in XmlDocPos.Measure: " + node.NodeType.ToString());
            }

            
            foreach (XmlNode child in node.ChildNodes)
            {
                count += Measure(child, expectWhitespace);

            }
            return count;
        }

        public static bool Compare(XmlNode node1, XmlNode node2)
        {
            if (node1.NodeType == XmlNodeType.Text)
            {
                if (node2.NodeType != XmlNodeType.Text)
                    return false;
                if (string.Compare(node1.Value, node2.Value, false) != 0)
                    return false;

            }

            if (node1.ChildNodes.Count != node2.ChildNodes.Count)
                return false;

            for (int i = 0; i < node1.ChildNodes.Count; i++)
            {
                if (!Compare(node1.ChildNodes[i], node2.ChildNodes[i]))
                    return false;
            }

            return true;
        }



    }

    public class DocumentRange
    {
        public int StartPosition;
        public int Length;

        public int EndPos() { return StartPosition + Length; }
    }

    public class SegmentDefinitionComparer : IComparer<SegmentDefinition>
    {

        public int Compare(SegmentDefinition x, SegmentDefinition y)
        {
            if (x.StartPosition < y.StartPosition)
                return -1;

            if (y.StartPosition < x.StartPosition)
                return 1;

            if (x.Length > y.Length)
                return -1;
            if (y.Length > x.Length)
                return 1;

            return 0;

        }
    }

    public class SegmentTreeNode
    {
        public SegmentDefinition SegmentDefinition;
        public List<SegmentTreeNode> ChildSegmentDefinitions = new List<SegmentTreeNode>();

        public List<DocumentRange> SubtractDescendants()
        {
            List<DocumentRange> result = new List<DocumentRange>();
            if (ChildSegmentDefinitions.Count == 0)
            {
                // Nothing to subtract
                DocumentRange r = new DocumentRange();
                r.StartPosition = SegmentDefinition.StartPosition;
                r.Length = SegmentDefinition.Length;
#if DEBUG
                //if (SegmentDefinition.Length == 0)
                //{
                //    string s = "debug";
                //}

#endif
                if (r.Length > 0)
                    result.Add(r);
                return result;
            }

            List<SegmentDefinition> descendants = new List<SegmentDefinition>();
            GetDescendants(descendants);
            descendants.Sort(new SegmentDefinitionComparer());

            // Construct list of ranges that is the union of all the descendants' ranges
            List<DocumentRange> union = new List<DocumentRange>();
            DocumentRange currentRange = null;
            foreach (var d in descendants)
            {
                if (currentRange == null)
                {
                    currentRange = new DocumentRange();
                    currentRange.StartPosition = d.StartPosition;
                    currentRange.Length = d.Length;
                    union.Add(currentRange);
                    continue;
                }

                System.Diagnostics.Debug.Assert(d.StartPosition >= currentRange.StartPosition);

                if (d.StartPosition >= (currentRange.EndPos()))
                {
                    currentRange = new DocumentRange();
                    currentRange.StartPosition = d.StartPosition;
                    currentRange.Length = d.Length;
                    union.Add(currentRange);
                    continue;

                }
                else


                    currentRange.Length = d.Length + (d.StartPosition - currentRange.StartPosition);

            }

            // Now invert list to arrive at list of 'gaps'
            int prevEnd = SegmentDefinition.StartPosition;

            foreach (var r in union)
            {
                if (r.StartPosition > prevEnd)
                {

                    DocumentRange r2 = new DocumentRange();
                    r2.StartPosition = prevEnd;
                    r2.Length = r.StartPosition - prevEnd;
                    if (r2.EndPos() > (SegmentDefinition.StartPosition + SegmentDefinition.Length))
                        r2.Length = SegmentDefinition.Length - (r2.StartPosition - SegmentDefinition.StartPosition);
#if DEBUG
                    //if (r2.Length == 0)
                    //{
                    //    string s = "debug";
                    //}

#endif
                    if (r2.Length > 0)
                        result.Add(r2);


                }
                if ((r.EndPos()) > prevEnd)
                    prevEnd = r.EndPos();

                if (prevEnd > (SegmentDefinition.StartPosition + SegmentDefinition.Length))
                    // can happen with cascading overlapped descendants
                    break;
            }

            if (prevEnd < (SegmentDefinition.StartPosition + SegmentDefinition.Length))
            {
                DocumentRange r2 = new DocumentRange();
                r2.StartPosition = prevEnd;
                r2.Length = (SegmentDefinition.StartPosition + SegmentDefinition.Length) - prevEnd;
                result.Add(r2);
#if DEBUG
                //if (r2.Length == 0)
                //{
                //    string s = "debug";
                //}

#endif

            }
            return result;
        }

        void GetDescendants(List<SegmentDefinition> descendants)
        {
            foreach (var c in ChildSegmentDefinitions)
            {
                descendants.Add(c.SegmentDefinition);
                c.GetDescendants(descendants);
            }
        }


        public static List<SegmentTreeNode> SegmentsToTree(List<SegmentDefinition> segments, bool flattenTree)
        {

            segments.Sort(new SegmentDefinitionComparer());

            Stack<SegmentTreeNode> stack = new Stack<SegmentTreeNode>();
            List<SegmentTreeNode> rootEntries = new List<SegmentTreeNode>();
            foreach (var seg in segments)
            {
                SegmentTreeNode node = new SegmentTreeNode();
                node.SegmentDefinition = seg;

                while (stack.Count > 0)
                {
                    var parent = stack.Peek();

                    if ((node.SegmentDefinition.StartPosition) <
                        (parent.SegmentDefinition.StartPosition + parent.SegmentDefinition.Length))
                        break;

                    stack.Pop();
                }

                if (stack.Count > 0)
                {
                    var parent = stack.Peek();
                    parent.ChildSegmentDefinitions.Add(node);
                    if (flattenTree)
                        rootEntries.Add(node);
                    stack.Push(node);
                }
                else
                {
                    rootEntries.Add(node);
                    stack.Push(node);
                }


            }

            return rootEntries;

        }

    }


}
