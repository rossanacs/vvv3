﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using EblaAPI;
using EblaImpl.Translation;

namespace EblaImpl.Calculator
{
    public class TranslationManager
    {
        private readonly ITranslationCalculatorFactory _factory;
        private readonly ITranslationCalculator _translationCalculator;

        public TranslationManager(ITranslationCalculatorFactory factory, ITranslationCalculator translationCalculator)
        {
            _factory = factory;
            _translationCalculator = translationCalculator;
        }

        public TranslationProbabilities GetTranslationProbabilities(MooreTranslationModel mooreStatus,
            SegmentTokens[] baseSegmentList,
            SegmentTokens[] versionSegmentList)
        {
            var translationProbabilities = _factory.GetTranslationCalculator(mooreStatus)
                .GetTranslationProbabilities(baseSegmentList, versionSegmentList);

            return translationProbabilities;
        }

    }

    public enum MooreTranslationModel
    {
        FirstPhase = 1,
        SecondPhase = 2,
        ThirdPhase = 3
    }

    public interface ITranslationCalculator
    {
        TranslationProbabilities GetTranslationProbabilities(SegmentTokens[] sourceSegmentList,
           SegmentTokens[] targetSegmentList);
    }

    public interface ITranslationCalculatorFactory
    {
        ITranslationCalculator GetTranslationCalculator(MooreTranslationModel mooreStatus);
    }

    public class DefaultTranslationCalculatorFactory : ITranslationCalculatorFactory
    {
        public ITranslationCalculator GetTranslationCalculator(MooreTranslationModel mooreStatus)
        {
            ITranslationCalculator calculator = null;
            switch (mooreStatus)
            {
                case MooreTranslationModel.FirstPhase:
                    calculator = new PoissonCalculator();
                    break;
                case MooreTranslationModel.SecondPhase:
                    calculator = new IbmCalculator();
                    break;
                case MooreTranslationModel.ThirdPhase:
                    throw new NotImplementedException();
                default:
                    throw new NotImplementedException();
            }

            return calculator;
        }

    }

    public class PoissonCalculator : ITranslationCalculator
    {
        private TokensProbability BaseTextTokensProbabilities { get; set; }

        private TokensProbability VersionTokensProbabilities { get; set; }

        private List<AlignmentVocabulary> BaseTextAlignmentVocabulary { get; set; }

        private List<AlignmentVocabulary> VersionAlignmentVocabulary { get; set; }

        public int MeanRatio { get; set; }

        public TokensProbability BaseTextTokensProbability { get; set; }

        public TokensProbability VersionTokensProbability { get; set; }

        public TranslationProbabilities GetTranslationProbabilities(SegmentTokens[] baseSegmentList,
            SegmentTokens[] versionSegmentList)
        {
            var probabilities = new TranslationProbabilities();
            probabilities.BaseProbability = CalculateTokensProbabilities(baseSegmentList);
            probabilities.VersionProbability = CalculateTokensProbabilities(versionSegmentList);

            return probabilities;
        }

        private TokensProbability CalculateTokensProbabilities(SegmentTokens[] segmentList)
        {
            var tokensProb = GetTokensProbabilities(segmentList);

            for (var i = 0; i < tokensProb.TokensProbabilityValues.Length; ++i)
            {
                var probability = tokensProb.TokensProbabilityValues[i] / tokensProb.AttributeValueOccurenceCount;
                tokensProb.TokensProbabilityValues[i] = probability;
            }
            tokensProb.Mean = (float)tokensProb.TotalAttributeValue / tokensProb.AttributeValueOccurenceCount;

            return tokensProb;
        }

        private static TokensProbability GetTokensProbabilities(SegmentTokens[] segmentList)
        {
            var tokensProb = new TokensProbability();

            var maxAttributeValue = segmentList.Max(t => t.TotalTokensValue) + 1;

            tokensProb.TokensProbabilityValues = new float[maxAttributeValue];
            foreach (var segment in segmentList)
            {
                tokensProb.TokensProbabilityValues[segment.TotalTokensValue] += 1;
                tokensProb.AttributeValueOccurenceCount++;
                tokensProb.TotalAttributeValue += segment.TotalTokensValue;
            }

            return tokensProb;
        }

        private double CalculateSegmentLikelihood(SegmentTokens[] segmentList, TokensProbability probability)
        {
            return segmentList.Sum(segment => -Math.Log(probability.TokensProbabilityValues[segment.TotalTokensValue]));
        }

        ///Calculates the score of alignments' tokens.
        private float CalculatePoissonDistributionScore(SegmentTokens[] segmentsBaseDocument, SegmentTokens[] segmentsVersionDocument)
        {
            var baseTotalTokens = CalculateSegmentLength(segmentsBaseDocument);
            var versionTotalTokens = CalculateSegmentLength(segmentsVersionDocument);
            var mean = baseTotalTokens * MeanRatio;

            var score = EblaHelpers.PoissonDistribution(mean, versionTotalTokens);
            return score;
        }


        ///Calculates the score of alignments' tokens.
        private static int CalculateSegmentLength(SegmentTokens[] segmentList)
        {
            return segmentList.Aggregate(0, (current, segment) => current + segment.TotalTokensValue);
        }

        private double CalculateSegmentLengthScore(SegmentTokens[] baseSegmentDefinitionTotalTokenses, SegmentTokens[] versionSegmentDefinitionTotalTokenses)
        {
            double score;
            if (baseSegmentDefinitionTotalTokenses.Length == 0 && versionSegmentDefinitionTotalTokenses.Length == 0)
            {
                score = 0.0d;
            }
            else if (baseSegmentDefinitionTotalTokenses.Length == 0)
            {
                score = CalculateSegmentLikelihood(versionSegmentDefinitionTotalTokenses, VersionTokensProbability);
            }
            else
            {
                score = CalculateSegmentLikelihood(baseSegmentDefinitionTotalTokenses, BaseTextTokensProbability);
                if (versionSegmentDefinitionTotalTokenses.Length > 0)
                {
                    score += CalculatePoissonDistributionScore(baseSegmentDefinitionTotalTokenses,
                        versionSegmentDefinitionTotalTokenses);
                }
            }
            return score;
        }
    }

    public class IbmCalculator : ITranslationCalculator
    {
        public TranslationProbabilities GetTranslationProbabilities(SegmentTokens[] baseSegmentList, SegmentTokens[] versionSegmentList)
        {
            throw new NotImplementedException();
        }
    }

}
