﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using EblaAPI;
using EblaImpl.Aligner;
using EblaImpl.Translation;

namespace EblaImpl.Calculator
{
    public class WordCalculator: ICalculator
    {
        private const float MinimumTranslationProbability = 1e-38f;

        internal float Mean { get; set; }
        internal TokensProbability BaseProbability { get; set; }
        internal TokensProbability VersionProbability { get; set; }
        internal List<AlignmentVocabulary> BaseVocabulary { get; set; }
        internal List<AlignmentVocabulary> VersionVocabulary { get; set; }
        internal TranslationData WordTranslationData { get; set; }
        internal List<BestAlignment> BestAlignments { get; set; }
        internal List<BestAlignment> InitialAlignments { get; set; }
        public TranslationDocument WordTranslationDocument { get; set; }

        public WordCalculator()
        {
            if (BaseProbability != null && VersionProbability != null)
                Mean = BaseProbability.Mean / VersionProbability.Mean;
        }

        public WordCalculator(TranslationDocument translationDocument)
        {
            BaseProbability = CalculateProbabilities(translationDocument.BaseDocumentSegmentsWids,
                translationDocument.BaseVocabulary);
            VersionProbability = CalculateProbabilities(translationDocument.VersionDocumentSegmentsWids,
                translationDocument.VersionVocabulary);
            if (BaseProbability != null && VersionProbability != null)
                Mean = BaseProbability.Mean / VersionProbability.Mean;
            BaseVocabulary = translationDocument.BaseVocabulary;
            VersionVocabulary = translationDocument.VersionVocabulary;
        }

        public float CalculateScore(SegmentTokens[] baseTokenses,
            SegmentTokens[] versionTokenses)
        {
            var translationUtil = new TranslationUtil();
            var baseWidList = translationUtil.CreateSegmentWidList(baseTokenses, BaseVocabulary, false);
            var versionWidList = translationUtil.CreateSegmentWidList(versionTokenses, VersionVocabulary, false);

            var score = CalculateScore(baseWidList, versionWidList);

            return score;
        }

        private float CalculateScore(List<List<int>> baseWidList, List<List<int>> versionWidList)
        {
            float score;
            if (baseWidList.Count == 0 && versionWidList.Count == 0)
            {
                score = 0.0f;
            }
            else if (baseWidList.Count == 0)
            {
                score = CalculateScore(versionWidList, VersionProbability);
            }
            else
            {
                score = CalculateScore(baseWidList, BaseProbability);
                if (versionWidList.Count <= 0) return score;
                var newBaseWidList = TranslationUtil.AddNullWidToList(baseWidList);
                score += CalculateTranslationScore(newBaseWidList, versionWidList, BaseProbability,
                    VersionProbability);
            }

            return score;
        }

        internal static float CalculateScore(List<List<int>> widList, TokensProbability probability)
        {
            var score =
                widList.Sum(
                    wids =>
                        wids.Where(wid => wid < probability.TokensProbabilityValues.Length)
                            .Sum(wid => -Math.Log(probability.TokensProbabilityValues[wid])));
            return (float) score;
        }

        internal float CalculateTranslationScore(List<List<int>> baseWidList, List<List<int>> versionWidList,
            TokensProbability baseProbability, TokensProbability versionProbability)
        {
            var score = -(float)Math.Log(1.0 / baseWidList.Count) * versionWidList.Count;
            foreach (var versionList in versionWidList)
            {
                var translationProbability = 0.0f;
                if (versionList != null)
                {
                    foreach (var baseList in baseWidList)
                    {
                        if (baseList != null)
                        {
                            translationProbability += CalculateTranslationScore(baseList, versionList);
                        }
                    }
                }

                translationProbability = Math.Max(translationProbability, MinimumTranslationProbability);
                score += -(float) Math.Log(translationProbability);
            }
            return score;
        }

        private float CalculateTranslationScore(List<int> baseWidList, List<int> versionWidList)
        {
            var score = 0.0f;
            foreach (var versionWid in versionWidList)
            {
                var translationProbability = 0.0d;
                foreach (var baseWid in baseWidList)
                {
                    var baseData = IbmModelAligner.GetBaseTranslationData(WordTranslationData, baseWid);
                    var versionData = baseData?.GetVersionTranslationData(versionWid);
                    if (versionData != null)
                    {
                        translationProbability += versionData.Probability;
                    }
                }

                score += (float)translationProbability;

            }

            return score;
        }

        public TokensProbability CalculateProbabilities(SegmentTokens[] segmentList,
            List<AlignmentVocabulary> vocabulary)
        {
            var tokensProb = GetTranslationProbabilities(segmentList, vocabulary);

            for (var i = 0; i < tokensProb.TokensProbabilityValues.Length; ++i)
            {
                var probability = tokensProb.TokensProbabilityValues[i] / tokensProb.AttributeValueOccurenceCount;
                tokensProb.TokensProbabilityValues[i] = probability;
            }
            tokensProb.Mean = 1.0f / tokensProb.AttributeValueOccurenceCount;

            return tokensProb;
        }

        private static TokensProbability GetTranslationProbabilities(SegmentTokens[] segmentList,
            List<AlignmentVocabulary> vocabulary)
        {
            var transProb = new TokensProbability { TokensProbabilityValues = new float[vocabulary.Count + 1] };
            foreach (var segment in segmentList)
            {
                foreach (var token in segment.Attributes)
                {
                    var alignmentToken = vocabulary.Find(t => t.Token == token.Name);
                    if (alignmentToken == null) continue;

                    transProb.TokensProbabilityValues[alignmentToken.Id] += 1;
                    transProb.AttributeValueOccurenceCount++;
                    transProb.TotalAttributeValue += int.Parse(token.Value);
                }
            }

            return transProb;
        }
    }
}
