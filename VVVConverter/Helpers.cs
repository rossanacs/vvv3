﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EblaAPI;
using EblaImpl;
using System.Xml;
using System.Data.SQLite;

namespace VVVConverter
{
    public class Helpers
    {
        static string _uname = "admin";
        static string _pwd = "VVVadmin1";

        static System.Collections.Specialized.NameValueCollection _settings = new System.Collections.Specialized.NameValueCollection()
        {
            {"ConnStr", GetConnectionString()}
        };

        static string GetConnectionString()
        {
            return @"Data Source=C:/temp/vvvconverter.db";
        }

        static public void InitTempDb()
        {
            ExecuteSchemaCommand("VVVConverter.CreateEblaDB-sqlite.sql", false);
        }

        internal static ICorpus GetCorpus(string name)
        {
            // TODO - consider caching in Session
            ICorpus corpus = EblaFactory.GetCorpus(_settings);

            if (!corpus.Open(name, _uname, _pwd))
                throw new Exception("You do not have permission to access the corpus.");

            return corpus;
        }

        internal static ICorpusStore GetCorpusStore()
        {
            ICorpusStore store = EblaFactory.GetCorpusStore(_settings);

            store.Open(_uname, _pwd);
            return store;
        }


        internal static IDocument GetBaseText(string corpusName)
        {
            IDocument doc = EblaFactory.GetDocument(_settings);

            if (!doc.OpenBaseText(corpusName, _uname, _pwd))
                throw new Exception("You do not have permission to access the document.");

            return doc;
        }

        internal static IDocument GetVersion(string corpusName, string versionName)
        {
            IDocument doc = EblaFactory.GetDocument(_settings);

            if (!doc.OpenVersion(corpusName, versionName, _uname, _pwd))
                throw new Exception("You do not have permission to access the document.");

            return doc;
        }

        internal static IAlignmentSet GetAlignmentSet(string corpusName, string versionName)
        {
            IAlignmentSet set = EblaFactory.GetAlignmentSet(_settings);

            if (!set.Open(corpusName, versionName, _uname, _pwd))
                throw new Exception("You do not have permission to access the alignment set.");

            return set;
        }


        internal static IDocument GetDocument(string corpusName, string nameIfVersion)
        {
            if (string.IsNullOrEmpty(nameIfVersion))
                return GetBaseText(corpusName);

            return GetVersion(corpusName, nameIfVersion);
        }

        //internal static System.Text.Encoding PrismEncodingToTextEncoding(PrismEncoding encoding)
        //{
        //    switch (encoding)
        //    {
        //        case PrismEncoding.ASCII:
        //            return System.Text.Encoding.ASCII;
        //        case PrismEncoding.UTF8:
        //            return System.Text.Encoding.UTF8;
        //        case PrismEncoding.UTF7:
        //            return System.Text.Encoding.UTF7;
        //        case PrismEncoding.UTF32:
        //            return System.Text.Encoding.UTF32;
        //        case PrismEncoding.Unicode:
        //            return System.Text.Encoding.Unicode;
        //        case PrismEncoding.BigEndianUnicode:
        //            return System.Text.Encoding.BigEndianUnicode;

        //    }

        //    throw new Exception("Unrecognised PrismEncoding: " + encoding.ToString());
        //}

        internal static XmlDocument XmlDocFromContent(string content)
        {
            string xml = @"<?xml version=""1.0"" ?><body>";

            xml += content;

            xml += "</body>";

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);
            return xmlDoc;
        }

        static private void ExecuteSchemaCommand(string schemaName, bool ignoreErrors)
        {
            string[] cmdChunks = ReadSchemaCommands(schemaName, ";");

            using (var cn = new SQLiteConnection(GetConnectionString()))
            {
                cn.Open();

                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = cn;
                    foreach (string chunk in cmdChunks)
                    {
                        cmd.CommandText = chunk.Trim(new char[] { '\r', '\n', '\t' });
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (SQLiteException)
                        {
                            if (!ignoreErrors)
                                throw;
                        }
                    }
                }

            }
        }

        static protected string[] ReadSchemaCommands(string schemaName, string separator)
        {
            System.IO.Stream schema =
                System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(schemaName);
            if (schema == null)
                throw new Exception("Embedded resource not found: " + schemaName);

            // read the stream into a string
            // TODO the hard-wired encoding part is ugly 
            var rdr = new System.IO.StreamReader(schema, Encoding.GetEncoding(1252));
            string createSchemaCmd = rdr.ReadToEnd();
            rdr.Close();

            // when executing the script as one command, we get exceptions "CREATE TRIGGER must be first command in a batch"
            // The workaround is to split the cmd at "GO" commands and execute each chunk individually
            string[] cmdChunks = createSchemaCmd.Split(new string[] { separator }, StringSplitOptions.RemoveEmptyEntries);

            return cmdChunks;
        }


    }
}
