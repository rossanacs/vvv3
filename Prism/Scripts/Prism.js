﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.
*/


var _siteUrl;

/////////////////
// Popup support
var PrismNS = PrismNS || {};

PrismNS.modalEnum = {
    okCancel: 0,
    yesNo: 1,
    closeOnly: 2
}

var _popupStack = new Array;
function PushPopupInfo(url, caption, type, argsObj, onShowFn, onSuccessFn, onBeforeSubmitFn) {
    var stackEntry = new Object();
    stackEntry.url = url;
    stackEntry.caption = caption;
    stackEntry.type = type;
    stackEntry.argsObj = argsObj;
    stackEntry.onShowFn = onShowFn;
    stackEntry.onSuccessFn = onSuccessFn;
    stackEntry.onBeforeSubmitFn = onBeforeSubmitFn;
    _popupStack.push(stackEntry);
}

function PopPopupInfo() {
    if (_popupStack.length == 0)
        return null;
    return _popupStack.pop();
}

function ClearPopupStack() {
    _popupStack = new Array;
}

function setModalOkState(enabled) {
    var dlgOk = $('#modalOk');
    var disval = 'disabled';
    if (!enabled)
        dlgOk.attr('disabled', 'disabled');
    else
        dlgOk.removeAttr('disabled');
}

function ProcessModalPop() {

    PopPopupInfo(); // get rid of entry for this popup
    var stackEntry = PopPopupInfo(); // any underneath?
    if (stackEntry) {

        swapModal(stackEntry.url, stackEntry.caption, stackEntry.type, stackEntry.argsObj, stackEntry.onShowFn, stackEntry.onSuccessFn, stackEntry.onBeforeSubmitFn);
    }
    else {
        $.modal.close(); 
        //window.location.reload();
    }


}

function CustomModalCancel(e) {
    ProcessModalPop();
}

function fixupModalAnchors(content) {
    $('.swapModal', content).click(function (e) {
        e.preventDefault();
        swapModal($(this).attr('href'), e.target.title, PrismNS.modalEnum.okCancel);
    });
    $('.swapCloseOnly', content).click(function (e) {
        e.preventDefault();
        swapModal($(this).attr('href'), e.target.title, PrismNS.modalEnum.closeOnly);
    });
    $('.swapConfirm', content).click(function (e) {
        e.preventDefault();
        swapModal($(this).attr('href'), e.target.title, PrismNS.modalEnum.yesNo);
    });


}

function fixupModalButtons(form, type) {
    var footer = form.children('#modalFooter');
    if (type == PrismNS.modalEnum.closeOnly) {
        footer.children('#modalOk').css('display', 'none');
        footer.children('#modalCancel').val('Close');
    } else {
        footer.children('#modalOk').css('display', '');
        footer.children('#modalOk').val(type == PrismNS.modalEnum.yesNo ? 'Yes' : 'OK');
        footer.children('#modalCancel').val(type == PrismNS.modalEnum.yesNo ? 'No' : 'Cancel');
    }

}

function ResponseResult(responseText) {
    // response from controller can either be JSON (to return data) or Content if
    // the dialog needs to be re-displayed (e.g. invalid input needs correcting).

    if (responseText.substring(0, 7) == 'result:') {
        // It's JSON - return it as the result
        return JSON.parse(responseText.substring(7, responseText.length));
    }
    return null;
}



function showModal(url, caption, type, argsObj, onShowFn, onSuccessFn, onBeforeSubmitFn) {
    ClearPopupStack();
    PushPopupInfo(url, caption, type, argsObj, onShowFn, onSuccessFn, onBeforeSubmitFn);
    $.ajax({ url: url,
        type: 'GET', data: argsObj, dataType: 'json', traditional: true, cache: false,
        complete: function (data, textStatus) {
            var m = $('#modal');
            m.children('#modalCaption').html(caption);
            var form = m.children('#modalForm');
            var content = form.children('#modalContent');
            content.html(data.responseText);
            fixupModalAnchors(content);
            fixupModalButtons(form, type);
            setModalOkState(true);
            m.modal({ focus: false, opacity: 75,
                onShow: function (d) {
                    if (onShowFn)
                        onShowFn();
                    form.unbind('submit');
                    form.ajaxForm({
                        url: url,
                        beforeSubmit: function (formdata, jqForm, options) {
                            if (!onBeforeSubmitFn)
                                return true;
                            var beforeSubmitResult = onBeforeSubmitFn(formdata, jqForm, options);
                            if (beforeSubmitResult == -1) {
                                $.modal.close();
                                return;
                            }

                            return (!!beforeSubmitResult);
                        },
                        beforeSerialize: function ($form, options) {
                            setModalOkState(false);
                        },
                        success: function (responseText, statusText, xhr, $form) {
                            setModalOkState(true);
                            var result = ResponseResult(responseText);
                            if (result) {
                                if (result.Succeeded) {
                                    $.modal.close();
                                    //window.location.reload();
                                    if (onSuccessFn)
                                        onSuccessFn();
                                    return;
                                }
                                fixupModalButtons(form, PrismNS.modalEnum.closeOnly);
                                responseText = result.ErrorMsg;

                            }

                            content.html(responseText);
                            fixupModalAnchors(content);

                            $.modal.update(m[0].offsetHeight, m[0].offsetWidth);
                        }
                    });
                    $('.focus', m).focus().select();
                }
            });
        }
    });
}



function swapModal(url, caption, type, argsObj, onShowFn, onSuccessFn, onBeforeSubmitFn) {
    PushPopupInfo(url, caption, type, argsObj, onShownFn, onSuccessFn, onBeforeSubmitFn);
    $.ajax({ url: url,
        type: 'GET', data: argsObj, dataType: 'json', traditional: true, cache: false,
        complete: function (data, textStatus) {
            var m = $('#modal');
            m.children('#modalCaption').html(caption);
            var form = m.children('#modalForm');
            var content = form.children('#modalContent');
            content.html(data.responseText);
            fixupModalAnchors(content);
            fixupModalButtons(form, type);
            $.modal.update(m[0].offsetHeight, m[0].offsetWidth);
            if (onShowFn)
                onShowFn();
            form.unbind('submit');
            setModalOkState(true);
            form.ajaxForm({
                target: '#modalContent',
                url: url,
                beforeSubmit: function (formdata, jqForm, options) {
                    if (!onBeforeSubmitFn)
                        return true;
                    var beforeSubmitResult = onBeforeSubmitFn(formdata, jqForm, options);
                    if (beforeSubmitResult == -1) {
                        ProcessModalPop();
                        return;
                    }

                    return (!!beforeSubmitResult);
                },
                beforeSerialize: function ($form, options) {
                    setModalOkState(false);
                },
                success: function (responseText, statusText, xhr, $form) {
                    setModalOkState(true);

                    var result = ResponseResult(responseText);
                    if (result) {
                        if (result.Succeeded) {
                            ProcessModalPop();
                            if (onSuccessFn)
                                onSuccessFn();
                            return;
                        }
                        fixupModalButtons(form, PrismNS.modalEnum.closeOnly);
                        responseText = result.ErrorMsg;

                    }

                    content.html(responseText);
                    fixupModalAnchors(content);
                    $.modal.update(m[0].offsetHeight, m[0].offsetWidth);
                }
            });
            $('.focus', m).focus().select();
        }
    });
}



/////////////////




var _corpusName = '';
var _versionName = '';




$(function () {

    _siteUrl = $('#siteUrl').val();

    $.ajaxSetup({
        cache: false
    });

    /////////////////
    // Popup support

    $('#modalCancel').click(function (e) { CustomModalCancel(e); });

    $('.modalCrossIcon').click(function (e) { CustomModalCancel(e); });


    $('.showModal').click(function (e) {
        e.preventDefault();
        showModal($(this).attr('href'), e.target.title, PrismNS.modalEnum.okCancel);
    });
    $('.showModalButton').click(function (e) {
        e.preventDefault();
        showModal($(this).attr('name'), e.target.title, PrismNS.modalEnum.okCancel);
    });
    $('.showModalCloseOnly').click(function (e) {
        e.preventDefault();
        showModal($(this).attr('href'), e.target.title, PrismNS.modalEnum.closeOnly);
    });
    $('.showModalButtonCloseOnly').click(function (e) {
        e.preventDefault();
        showModal($(this).attr('name'), e.target.title, PrismNS.modalEnum.closeOnly);
    });
    $('.showConfirm').click(function (e) {
        e.preventDefault();
        showModal($(this).attr('href'), e.target.title, PrismNS.modalEnum.yesNo);
    });

    /////////////////




    _corpusName = $('#corpusname').val();
    _versionName = $('#versionname').val();
    $(document).ajaxStop($.unblockUI); 
});

function CallController(controllerUrl, argsObj) {
    var result;
    $.ajax({ url: controllerUrl, type: 'POST', data: argsObj, dataType: 'json', async: false, traditional: true,
        complete: function (data, textStatus) {
            if (textStatus == 'error') {
                alert(data.responseText);
                return null;
            }
            result = JSON.parse(data.responseText);

        }
    });

    return result;
}

function CallControllerAsync(controllerUrl, argsObj, resultfn, errorfn) {
    var result;$.blockUI({ fadeIn: 50, fadeOut: 50, overlayCSS: {opacity: 0.0} });

    $.ajax({ url: controllerUrl, type: 'POST', data: argsObj, dataType: 'json', async: true, traditional: true,
        complete: function (data, textStatus) {
            if (textStatus == 'error') {
                if (errorfn)
                    errorfn(data);
                else
                    alert(data.responseText);
                return;
            }
            result = JSON.parse(data.responseText);
            if (resultfn)
                resultfn(result);
        }
    });
    
}
