function createNav( id ) {

    var actBar = new Array(),
        actBarDesc = new Array(),
        actTextLength = new Array(),
        actBarSceneNum = new Array(),
        actBarTextWidth = 0,
        actBarY = 0,
        actBarScale = 0,
        sceneBar = new Array(),
        sceneBarDesc = new Array(),
        sceneBarID = 0,
        sceneBarDescID = 0,
        sceneBarY = 0,
        sceneBarScale = 0,
        navWidth = 350,
        lastActiveBar = 'nav_' + id + '_0', //standard active is first act
        lastActiveScene = 'nav_' + id + '_0_0'; //standard active ist first scene of first act

  // get structure from json id
    $.ajax({
      type: "GET",
      url: './input/text.json',
      success: function (data) {
      var data  = $.parseJSON( data );
        createActBar(id, data);
      },
      error: function () {
        alert('can not load text');
      }
    });



    function createActBar(id, data) {
      
      // reset variables
      actBarTextWidth = 0;

      // init layers
      layerAct[ id ] = new Kinetic.Layer();
      layerActBarDesc[ id ] = new Kinetic.Layer();
      layerScene[ id ] = new Kinetic.Layer();
      layerSceneBarDesc[ id] = new Kinetic.Layer();

      // get number of acts
      var actNumb = data.id[ id ].structure.act.length;

      // get text length for every act
      for (var i=0; i < actNumb; i++) { 
          // init
          actTextLength[ i ] = 0;

          // calculate act size by getting all scene character length
          var sceneNumb = data.id[ id ].structure.act[ i ].scene.length;
          for(var j=0; j < sceneNumb; j++) {
            actTextLength[ i ] += data.id[ id ].structure.act[ i ].scene[ j ].textLength;
          }
      }

      // create rect for every act
      for (var i=0; i < actNumb; i++) {           

          actBar[ i ] = 0;
          actBarDesc[ i ] = 0;
          var actID = 'nav_' + id + '_' + i;
          
          // draw rect 
          actBar[ actID ] = new Kinetic.Rect({   
              x: 0,
              y: actBarY - 25,
              width: navBarWidthMax,
              height: actBarHeight,
              fill: actBarFill,
              name: data.id[ id ].structure.act[i].name,
              id: actID
              //$(speeches[i]).attr('id')
            });
          
             // init text object
            actBarDesc[ actID ] = new Kinetic.Text({
                text: "",
                fontFamily: fontNav,
                fontSize: 10,
                fontStyle: "normal",
                padding: 0,
                textFill: actBarDescFill,
                //fill: "black",
                alpha: 1,
                connectorible: false,
                align: "left",
                verticalAlign: "top"
            });

            // show description text 
            actBarDesc[ actID ].setText( actBar[ actID ].getName() );
            actBarDesc[ actID ].setPosition( actBar[ actID ].getPosition().x + barPaddingLeft, actBar[ actID ].getPosition().y + barPaddingTop);


            // event listener
            actBar[ actID ].on("mousemove", function() {
                document.body.style.cursor = "pointer";
                this.setFill(actBarFillHover);
                actBarDesc[ this.getID() ].setTextFill(actBarDescFillHover);
                layerAct[ id ].draw();
                layerActBarDesc[ id ].draw();  
            });
            
            actBar[ actID ].on("mouseout", function() {
                document.body.style.cursor = "default";
                this.setFill(actBarFill);
                actBarDesc[ this.getID() ].setTextFill(actBarDescFill);
                // the selected one should stay marked
                actBar[ lastActiveBar ].setFill(actBarFillSelected);
                actBarDesc[ lastActiveBar  ].setTextFill(actBarDescFillSelected);
                layerAct[ id ].draw();
                layerActBarDesc[ id ].draw(); 
            });

            actBar[ actID ].on("click", function(e) {
                document.body.style.cursor = "pointer";
                e.preventDefault();
                
                sceneOnClicked( this.getID() );


              
            });

            //add act bar and desc to canvas
            layerAct[ id ].add( actBar[ actID ] );
            layerActBarDesc[ id ].add( actBarDesc[ actID ] );
            layerAct[ id ].setPosition( 0, titleTextHeight);
            layerActBarDesc[ id ].setPosition( 0, titleTextHeight);
            
            //create releated scenebars
            createSceneBar(id, data, actBarY, actID, i);
            
            // increase the y position of the act bar
            actBarY += actBarHeight + actBarMargin;

        }

        //add layer with act bar to nav stage
        navStage[ id ].add(layerAct[ id ]); 
        navStage[ id ].add(layerActBarDesc[ id ]); 

        // mark active bar
        actBar[ lastActiveBar ].setFill(actBarFillSelected);
        actBar[ lastActiveBar ].setWidth(navBarWidthMin);
        //actBarDesc[ lastActiveBar ].setTextFill(actBarDescFillSelected);
        actBarDesc[ lastActiveBar ].hide();
        layerAct[ id ].draw(); 
        layerActBarDesc[ id ].draw();  
        
        // mark active scene
        sceneBar[ lastActiveScene ].setFill(actBarFillSelected);
        for(var i=0; i < actBarSceneNum[ lastActiveBar ] ;i++) {
            sceneBar[ lastActiveBar + '_' + i ].setWidth(navBarWidthMax);
            sceneBar[ lastActiveBar + '_' + i ].setX( actBar[ lastActiveBar ].getX() + actBar[ lastActiveBar ].getWidth() + actSceneMargin );
            sceneBarDesc[ lastActiveBar + '_' + i ].show();
        }
        sceneBarDesc[ lastActiveScene ].setTextFill(actBarDescFillSelected);
        layerScene[ id ].draw(); 
        layerSceneBarDesc[ id ].draw();
    }



    function createSceneBar(id, data, actBarY, actID, actNum) {

      // reset height
      sceneBarY = actBarY;

      // get number of scenes of the active Act
        actBarSceneNum[ actID ] = data.id[ id ].structure.act[ actNum ].scene.length;
        //calc height of each scene depending on the number of scenes
        var sceneBarHeight = (actBarHeight - sceneBarMargin * actBarSceneNum[ actID ] + sceneBarMargin ) / actBarSceneNum[ actID ];

      // create rect for every scene
      for (var i=0; i < actBarSceneNum[ actID ]; i++) {

          var sceneID =  actID + '_' + i;
          // calculate scene size by getting scene character length
          //var sceneTextLength = 0;
          //sceneTextLength = data.id[ id ].structure.act[ activeAct ].scene[ i ].textLength;

          // draw rect 
          sceneBar[ sceneID ] = new Kinetic.Rect({   
            //x: actBarHeight + actSceneBarMargin + titleTextHeight,
            x: navBarWidthMax + actSceneMargin,
            y: sceneBarY,
            width: navBarWidthMin,
            height: sceneBarHeight,
            fill: actBarFill,
            name: data.id[ id ].structure.act[ actNum ].scene[ i ].name,
            id: sceneID
          });


           // init text object
          sceneBarDesc[ sceneID ] = new Kinetic.Text({
              text: "",
              fontFamily: fontNav,
              fontSize: 10,
              padding: 0,
              textFill: actBarDescFill,
              alpha: 1,
              connectorible: false,
              align: "left",
              verticalAlign: "top"
          });

          // show description text 
          sceneBarDesc[ sceneID ].setText( sceneBar[ sceneID ].getName() );
          sceneBarDesc[ sceneID ].setPosition( sceneBar[ sceneID ].getPosition().x + barPaddingLeft, sceneBar[ sceneID ].getPosition().y + barPaddingTop);

           // event listener
            sceneBar[ sceneID ].on("mousemove", function() {
                document.body.style.cursor = "pointer";
                this.setFill(actBarFillHover);
                sceneBarDesc[ this.getID()  ].setTextFill(actBarDescFillHover);
                layerScene[ id ].draw();
                layerSceneBarDesc[ id ].draw();  
            });
            
            sceneBar[ sceneID ].on("mouseout", function() {
                document.body.style.cursor = "default";
                this.setFill(actBarFill);
                sceneBarDesc[ this.getID()  ].setTextFill(actBarDescFill);
                // the selected one should stay marked
                sceneBar[ lastActiveScene ].setFill(actBarFillSelected);
                sceneBarDesc[ lastActiveScene  ].setTextFill(actBarDescFillSelected);
                layerScene[ id ].draw();
                layerSceneBarDesc[ id ].draw(); 
            });

            sceneBar[ sceneID ].on("click", function(e) {
              document.body.style.cursor = "pointer";
              e.preventDefault();
                
                // short click direct on scene
              sceneOnClicked( actID );

              sceneBar[ lastActiveScene ].setFill(actBarFill);
              sceneBarDesc[ lastActiveScene  ].setTextFill(actBarDescFill);
              lastActiveScene = this.getID();
              this.setFill(actBarFillSelected);
              layerScene[ id ].draw();
              layerSceneBarDesc[ id ].draw();
              
            });

            //bug from kinetic.js
            sceneBarDesc[ sceneID ].setX( navBarWidthMin + actSceneMargin *2 );
            
            //standard hide description
            sceneBarDesc[ sceneID ].hide();
               
          // increase the width of the act bar
          sceneBarY += sceneBarHeight + sceneBarMargin;
          
          //add  bar to canvas
          layerScene[ id ].add( sceneBar[ sceneID ] );
          layerSceneBarDesc[ id ].add( sceneBarDesc[ sceneID ] );
          
          //increase index 
          sceneBarID++;
          sceneBarDescID++;
        }
        //add layer with act bar to nav stage
        navStage[ id ].add(layerScene[ id ]);
        navStage[ id ].add(layerSceneBarDesc[ id ]);
    }
    
    
    function sceneOnClicked( t ) { 

      //reset old actBar
      actBar[ lastActiveBar ].setFill(actBarFill);
      actBar[ lastActiveBar ].setWidth(navBarWidthMax);
      actBarDesc[ lastActiveBar ].show();
      actBarDesc[ lastActiveBar  ].setTextFill(actBarDescFill);             
      
      //reset old SceneBar
      sceneBar[ lastActiveScene ].setFill(actBarFill);
      //change width of all scenes of old act
      for(var i=0; i < actBarSceneNum[ lastActiveBar ] ;i++) {
        sceneBar[ lastActiveBar + '_' + i ].setWidth(navBarWidthMin);
        sceneBar[ lastActiveBar + '_' + i ].setX( actBar[ lastActiveBar ].getX() + actBar[ lastActiveBar ].getWidth() + actSceneMargin );
        sceneBarDesc[ lastActiveBar + '_' + i ].hide();
      }
      sceneBarDesc[ lastActiveScene ].setTextFill(actBarDescFill);
                
               
      //select new actBAr
      lastActiveBar = t;
      actBar[ lastActiveBar ].setFill(actBarFillSelected);
      actBar[ lastActiveBar ].setWidth(navBarWidthMin);
      actBarDesc[ lastActiveBar ].hide();
      layerAct[ id ].draw();
      layerActBarDesc[ id ].draw();
      
       //select new actScene
      lastActiveScene = t + '_0'; //standard select first scene
      sceneBar[ lastActiveScene ].setFill(actBarFillSelected);
      for(var i=0; i < actBarSceneNum[ lastActiveBar ] ;i++) {
        sceneBar[ lastActiveBar + '_' + i ].setWidth(navBarWidthMax);
        sceneBar[ lastActiveBar + '_' + i ].setX( actBar[ lastActiveBar ].getX() + actBar[ lastActiveBar ].getWidth() + actSceneMargin );
        sceneBarDesc[ lastActiveBar + '_' + i ].show();
      }
      sceneBarDesc[ lastActiveScene ].setTextFill(actBarDescFillSelected);
      
      layerScene[ id ].draw();
      layerSceneBarDesc[ id ].draw(); 
    }
    

}

              
              

function showHideNav( id ) {

  var nav = $("#nav_canvas_" +id );
  var hideNav = $("#hideNav_"+id );
  var content = $("#content_" +id );
  var f = $('#filter_canvas_'+id);
  var hideFil = $('#hideFilter_'+id)
    
    //if filter is visible, hide
    if( f.hasClass('visible') ) {
        f.removeClass('visible').addClass('notVisible'); hideFil.removeClass('visible').addClass('notVisible');
        f.animate({ 'height': 0 }, 100, scrollStyle);
    }
    /*if (f.hasClass('notVisible') && nav.hasClass('notVisible')) {
        content.animate({ left: $("#content_" +id ).position().left + 100}, 100, scrollStyle, function() {   if(filterStatus) updateConnector( id, 'nav'); }); 
    }*/

  if( nav.hasClass('visible') ) { 
    nav.removeClass('visible').addClass('notVisible'); hideNav.removeClass('visible').addClass('notVisible');
    nav.animate({ 'height': 0}, 100, scrollStyle);
  }
  else {  
    nav.removeClass('notVisible').addClass('visible'); hideNav.removeClass('notVisible').addClass('visible');
    nav.delay(100).animate({ 'height': sidebarHeight }, 100, scrollStyle);
  }
}

function showHideFilter( id ) {

  var t = $("#txt_" +id );
  var f = $('#filter_canvas_'+id);
  var hideFil = $("#hideFilter_"+id );
  var nav = $("#nav_canvas_" +id );
  var content = $("#content_" +id );
  var hideNav = $("#hideNav_"+id );

    //if nav is visible, hide nav content and show filter
    if( nav.hasClass('visible') ) {
        nav.removeClass('visible').addClass('notVisible'); hideNav.removeClass('visible').addClass('notVisible');
        nav.animate({ 'height': 0 }, 100, scrollStyle);
    }
    /*if (f.hasClass('notVisible') && nav.hasClass('notVisible')) {
        content.animate({ left:  $("#content_" +id ) + 100}, 100, scrollStyle, function() {   if(filterStatus) updateConnector( id, 'nav'); }); 
    }*/    
    
    if( f.hasClass('visible') ) { 
      f.removeClass('visible').addClass('notVisible'); hideFil.removeClass('visible').addClass('notVisible');
      f.animate({ 'height': 0 }, 100, scrollStyle); 
    }
    else { 
      f.removeClass('notVisible').addClass('visible'); hideFil.addClass('visible').removeClass('notVisible');
      f.delay(100).animate({ 'height': '100%' }, 100, scrollStyle);
    }

}




