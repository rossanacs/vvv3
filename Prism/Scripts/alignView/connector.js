function createConnector( id, sID ) { 
 layerCurve[ 0 ].clear();
 
   var v = new Array();
   v[0] = 'SegmentIDsInBaseText';
   v[1] = 'SegmentIDsInVersion';
   var alignTo;
   
    //find the aligned part

    // get alignments
    var args = {
      BaseTextModel: {
        CorpusName: _corpusName,
      },
      VersionModel: {
        CorpusName: _corpusName,
        VersionName: _versionName
      }
    }

    $.vvv.getAlignments( args, function( data ) {
      var res = _.find(data.data.Alignments, function (arr) { return arr[ v[id] ][0] == sID; })
    
      if(res) {
        //found counterpart
        var temp=0;
        if(id==0) {temp = 1;} else {temp = 0;}
        alignTo = res[ v[temp] ][0];
        
        align(alignTo, temp);
      }
      else {
        //no alignment found
        filterStatus = false;
        layerCurve[ 0 ].clear();

        var temp=0;
        if(id==0) {temp = 1;} else {temp = 0;}

        // remove textSelected class from all block quotes
        $( '.' + translation[temp].alias ).children( 'blockquote' ).removeClass( 'textSelected' );
        _.each( speakerBar, function( bar, key ) {
          if ( !_.isEqual( bar, 0 ) ) {
            bar.setFill( filterNormal );
          }
        });

        //draw layers again
        layerFilter[ temp ].draw();
        layerSpeaker[ temp ].draw();
      }
    }, function( err ) {
      debug.error( err.msg );
    });

    function align(alignTo, version) {
	    //console.log();
	    //mark Text
	    //be sure its the first span
		 alignTo = $('.txt_'+version+' span[data-eblasegid='+alignTo+']').parent().children("span").attr('data-eblasegid');


    	//reset old selected
        /*if(translation[version].getActiveTextItem()) $( translation[ version ].getActiveTextItem() ).parent().children().removeClass('textSelected');
        if(translation[version].getActiveFilterItem()) translation[ version ].getActiveFilterItem().setFill(filterNormal);
        if(translation[version].getActiveNameItem()) translation[ version ].getActiveNameItem().hide();*/

        //save new selected
        translation[version].setActiveTextItem( 'span[data-eblasegid='+alignTo+']' );
        translation[version].setActiveFilterItem( speakerBar[ alignTo ] );
        translation[version].setActiveNameItem( speaker[ alignTo ] );
        lastActiveFilterItem = speakerBar[ alignTo ];

		//style new selected
        $(translation[version].getActiveTextItem() ).parent().addClass('textSelected');
        speakerBar[ alignTo ].setFill(filterSelected);
           
		//draw layers again
        layerFilter[ version ].draw();
        layerSpeaker[ version ].draw();
        
        
        //scroll to text part
    	$('.txt_'+version+' span[data-eblasegid='+alignTo+']').ScrollTo({ duration: scrollDuration, easing: scrollStyle} ); 
       
         //show connector 
          filterStatus = true;              
        var conWidth = $(".txt_1").offset().left - ( $(".txt_0 .text").offset().left + $(".txt_0 .text").outerWidth() + 200 );
		connectorWidth = conWidth;
		   
	      updateConnector( parseInt( id ) );
	}

   
}

function updateConnector( id ) {


    var contentTop = 132;
    var paddingTopBot = 11;
    
     if( id < currentID-1 ) { id = id; } else {id = id -1;}
     var connectID = parseInt(id+1);
         
      var translation1_right  = $(translation[ id ].getActiveTextItem()).parent().offset().left + $(translation[ id ].getActiveTextItem()).parent().outerWidth() + 75 -  contentMargin;
      var translation1_top = $(translation[ id ].getActiveTextItem()).parent().offset().top - contentTop;
      var translation1_bottom = $(translation[ id ].getActiveTextItem()).parent().offset().top + $(translation[ id ].getActiveTextItem()).parent().height() - contentTop + paddingTopBot;

      var translation2_left  = $(translation[ connectID ].getActiveTextItem()).parent().offset().left  ;
      var translation2_top = $(translation[ connectID ].getActiveTextItem()).parent().offset().top - contentTop;
      var translation2_bottom = $(translation[ connectID ].getActiveTextItem()).parent().offset().top + $(translation[ connectID ].getActiveTextItem()).parent().height() - contentTop + paddingTopBot;

      var translation2_middle = $(translation[ connectID ].getActiveTextItem()).parent().offset().top + ( $(translation[ connectID ].getActiveTextItem()).parent().height() / 2 ) - contentTop;


      //layerCurve[ id ].clear();
      layerCurve[ id ].removeChildren();

      var line = new Kinetic.Shape({
        drawFunc: function() {
          var context = this.getContext();
          context.beginPath();
          //context.moveTo(0, translation1_top);
          //context.lineTo(0, translation1_bottom);
          //context.lineTo(connectorWidth, translation2_middle);
          //context.bezierCurveTo( connectorWidth,translation1_top,  0,translation2_middle,  connectorWidth,translation2_middle );
          //context.bezierCurveTo(  0, translation2_middle,  connectorWidth,translation1_bottom,  0,translation1_bottom );

          context.moveTo(0, translation1_top);
          context.lineTo(0, translation1_bottom);
          context.lineTo(connectorWidth+ contentMargin, translation2_bottom);
          context.lineTo(connectorWidth+ contentMargin, translation2_top);

          context.closePath();
          this.fillStroke();
        },
         fill: textHighlight,
        //stroke: "black",
        //strokeWidth: 0
      });
     layerCurve[ id ].add(line);
     layerCurve[ id ].draw();

}