﻿

var PrismNS = PrismNS || {};

PrismNS.VivDistributionChart = {};

PrismNS.VivDistributionChart.plot = null;

$(function () {

    EblaNS.FillVariationTypeSelectElm('#variation-type-select');

    var $vs = $('#variation-type-select');

    // switch viv type show on select
    $vs.change(function (event) {

        PrismNS.VivDistributionChart.GetChartData(false);

    });



    PrismNS.VivDistributionChart.GetChartData(true);

    $('#zoomout').click(function (e) { PrismNS.VivDistributionChart.ShowChart(null, null, null, null); });


});

var previousPoint = null;
var previousPointDisplayTime = null;

PrismNS.VivDistributionChart.chartSeries = null;
PrismNS.VivDistributionChart.barWidth = null;


PrismNS.VivDistributionChart.GetChartData = function (firsttime) {
    var corpusname = $('#corpusname').val();

    var $vs = $('#variation-type-select');

    //alert($vs.val());

    var url = _siteUrl + "Charts/GetVivDistributionData";
    var args = { CorpusName: corpusname, metricTypeVal: parseInt($vs.val()), barWidth: 0 };


    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {

                PrismNS.VivDistributionChart.PrepareChartData(result);

                PrismNS.VivDistributionChart.ShowChart(null, null, null, null);
                return;

            }
            alert('Retrieving chart data failed: ' + result.ErrorMsg);

        }

    });

}



PrismNS.VivDistributionChart.PrepareChartData = function (chartdata) {
    var points = [];
    for (var i = 0; i < chartdata.Bars.length; i++) {
        points.push([chartdata.BarWidth * i, chartdata.Bars[i].BaseTextSegIds.length]);
    }

    //    for (var i = 0; i < chartdata.BaseTextSegmentLengths.length; i++) {
    //        var segLength = chartdata.BaseTextSegmentLengths[i];
    //        var vivVals = chartdata.VivValues[i];

    //        var useAverage = true;
    //        if (useAverage) {
    //            var acc = 0.0;
    //            if (vivVals.length > 0) {
    //                for (var j = 0; j < vivVals.length; j++) {

    //                    acc += vivVals[j];
    //                }

    //                points.push([segLength, acc / vivVals.length]);
    //            }


    //        }
    //        else {
    //            for (var j = 0; j < vivVals.length; j++) {
    //                points.push([segLength, vivVals[j]]);

    //            }
    //        }


    //    }

    PrismNS.VivDistributionChart.chartSeries = { /*label: 'test',*/ data: points };

    PrismNS.VivDistributionChart.barWidth = chartdata.BarWidth;
}


PrismNS.VivDistributionChart.ShowChart = function (xaxismin, xaxismax, yaxismin, yaxismax) {


    var seriesList = new Array();

    seriesList.push(PrismNS.VivDistributionChart.chartSeries);


    var options = {
        series: { lines: { show: false }, bars: { show: true, barWidth: PrismNS.VivDistributionChart.barWidth} },
        xaxes: [{ position: 'top'}],

        grid: { hoverable: true} //,

            ,
        selection: { mode: "xy" }

    };

    if (xaxismin != null) {
        options.xaxis = { min: xaxismin, max: xaxismax };
        options.yaxis = { min: yaxismin, max: yaxismax };
    }

    PrismNS.Utils.EnableButton('zoomout', xaxismin != null);

    PrismNS.VivDistributionChart.plot = $.plot($('#chart'), seriesList, options);

    var ctx = PrismNS.VivDistributionChart.plot.getCanvas().getContext("2d");
    ctx.textAlign = 'center';

    $("#chart").bind("plotselected", function (event, ranges) {

        PrismNS.VivDistributionChart.ShowChart(ranges.xaxis.from, ranges.xaxis.to, ranges.yaxis.from, ranges.yaxis.to);

    });

}


function showTooltip(x, y, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y + 5,
        left: x + 5,
        border: '1px solid #fdd',
        padding: '2px',
        'background-color': '#fee',
        opacity: 0.80
    }).appendTo("body").fadeIn(200);
}
