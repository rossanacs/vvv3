var d3TargetSelector = '#alignmentviz',
    $d3Target,
    d3Width,
    d3Height,
    d3CellWidth = 70,
    d3BottomPadding = 100,
    d3PaddingRight = 10,
    alignmentViz;

var typeBasetext = 'basetext',
    typeVersionText = 'versiontext',
    baseTextLength,
    minLengthSpeech,
    maxLengthSpeech,
    speechHeight,
    baseTextSegments,
    versionTextSegments,
    connectionElements;
    
/**
* DOC READY!
**/
$( document ).ready( function() {
  var nav = $( '.navbar' ).outerHeight() + $( '.subnav' ).outerHeight();
  var h = window.innerHeight - nav - 65;
  $( '.scroll-wrapper' ).css( 'height', h );

  /**
    loads basetext segments by default
    we want the segment attributes, so we pass it as an option
  */
  $.vvv.getSegments( { includeAttribs: true }, function( data ) {
    // success
    // sort on segment start
    segments = _.sortBy( data.segments, function( segment ) { return segment.startPos } );
    segments = _.reject( segments, function( segment ) { return segment.length == 0; } );
    
    // get shortest speech
    minLengthSpeech = _.min( data.segments, function( segment ) { return segment.length; } );
    // get longest speech
    maxLengthSpeech = _.max( data.segments, function( segment ) { return segment.length; } );
    // get speech height mapping
    speechHeight = d3.scale.linear().domain( [minLengthSpeech.length, maxLengthSpeech.length] ).range( [5, 30] );
    // add x-pos & width info & calc speech height
    var totalheight = 0;
    _.each( segments, function( segment ) {
      segment.pos = {
        x: 0
      },
      segment.size = {
        w: d3CellWidth,
        h: speechHeight( segment.length )
      },
      totalheight += segment.size.h;
    }, function (msg) {alert(msg);});

    $d3Target = $( d3TargetSelector );
    d3Width = $d3Target.width(),
    d3Height = totalheight + d3BottomPadding;

    // create svg canvas
    alignmentViz = d3.select( d3TargetSelector )
      .append( 'svg:svg' )
      .attr( 'width', d3Width )
      .attr( 'height', d3Height );

    // finally, create segments
    baseTextSegments = createSegments( alignmentViz, segments, typeBasetext );
    refresh();

    /**
    * version selector change handler
    **/
    $( '#versionlist form select' ).change( function( event ) {
      var selectedVersion = event.target.value;
      if ( !_.isUndefined( selectedVersion ) && selectedVersion.length > 0) {

        var args = {
          CorpusName: _corpusName,
          VersionName: selectedVersion,
          includeAttribs: true
        }

        // load version text
        $.vvv.getText( args, function( data ) {
          $( '#versiontext' ).html( data );

          // load version segments
          $.vvv.getSegments( args, function( data ) {
            // success
            // sort on segment start
            segments = _.sortBy( data.segments, function( segment ) { return segment.startPos } );
            var totalheight = 0;
            _.each( segments, function( segment ) {
              segment.pos = {
                x: d3Width - d3CellWidth - d3PaddingRight
              },
              segment.size = {
                w: d3CellWidth,
                h: speechHeight( segment.length )
              },
              totalheight += segment.size.h;
            });
            
            if ( totalheight > d3Height - d3BottomPadding ) {
              d3Height = totalheight + d3BottomPadding;
              alignmentViz.attr( 'height', d3Height );
            }

            // create versiontext segments
            versionTextSegments = createSegments( alignmentViz, segments, typeVersionText );

            // get alignments
            var args = {
              BaseTextModel: {
                CorpusName: _corpusName,
              },
              VersionModel: {
                CorpusName: _corpusName,
                VersionName: selectedVersion
              }
            }

            $.vvv.getAlignments( args, function( data ) {
              connectionElements = createConnections( alignmentViz, data );
              refresh();

              alignmentViz.selectAll( '.selected' )
                .each( function( segment ) {
                  d3.select( this ).classed( 'selected', false );
                  toggleSegmentSelect.call( this, segment );
                });
            }, function( err ) {
              debug.error( err );
              alert(error.msg);
            });
            
          }, function( error ) {
            // error
            debug.error( error.msg );
            alert(error.msg);
          });
        }, function( error ) {
          debug.error( error.msg );
          alert(error.msg);
        });
      }
    });
  }, function( error ) {
    // error
    debug.error( error.msg );
    alert(error.msg);
  });
});

function refresh() {
  //$(".nano").nanoScroller();
}

// expects x-pos & width properties being attached to 
// the data beforehand
function createSegments( target, data, type )
{
  target.selectAll( 'rect[type=' + type + ']' ).remove();

  var segments = target.selectAll( 'rect[type=' + type + ']' )
    .data( data );

  segments.enter().append( 'rect' )
    .attr( 'width', function( d ) { return d.size.w; })
    .attr( 'height', 0 )
    .attr( 'x', function( d, i ) { return d.pos.x; })
    .attr( 'y', function( d, i ) {
      if ( i == 0 ) {
        d.pos.y = 0;
        return 0;
      }
      else {
        var pd = d3.select( this.previousSibling ).data()[0];
        d.pos.y = pd.pos.y + pd.size.h;
        return d.pos.y;
      }
    })
    .attr( 'type', type )
    .attr( 'data-segmentid', function( d ) { return d.ID; } )
    .on( 'mouseover', highlightSegment )
    .on( 'mouseout', unhighlightAll )
    .on( 'mousedown', toggleSegmentSelect );

  segments.transition()
    .duration( 1000 )
    .attr( 'height', function( d ) { return d.size.h; });
  return segments;
}

function createConnections( target, data )
{
  target.selectAll( 'polygon' ).remove();

  if ( data.status == 200 ) {
    // we need to take part (copy) one-to-many alignments
    // for connection creation
    var alignments = data.data.Alignments;
    var copies = [];
    var removeIndices = [];
    _.each( alignments, function( alignment, i ) {
      if ( alignment.SegmentIDsInBaseText.length > alignment.SegmentIDsInVersion.length ) {
        removeIndices.push( i );
        _.each( alignment.SegmentIDsInBaseText, function( segmentId, i ) {
          var copy = _.extend( {}, alignment );
          copy.SegmentIDsInBaseText = [alignment.SegmentIDsInBaseText[i]];
          copy.SegmentIDsInVersion = [alignment.SegmentIDsInVersion[i % alignment.SegmentIDsInVersion.length]];
          copies.push( copy );
        });
      }
      else if ( alignment.SegmentIDsInBaseText.length < alignment.SegmentIDsInVersion.length ) {
        removeIndices.push( i );
        _.each( alignment.SegmentIDsInVersion, function( segmentId, i ) {
          var copy = _.extend( {}, alignment );
          copy.SegmentIDsInBaseText = [alignment.SegmentIDsInBaseText[i % alignment.SegmentIDsInBaseText.length]];
          copy.SegmentIDsInVersion = [alignment.SegmentIDsInVersion[i]];
          copies.push( copy );
        });
      }
    });
    
    _.each( removeIndices, function( i ) {
      alignments.splice( i, 1 );
    });
    alignments = _.union( alignments, copies );

    var elements = target.selectAll( 'polygon' )
      .data( alignments );

    elements.enter().append( 'polygon' )
      .attr( 'points', function( alignment ) {
        var x1 = d3CellWidth,
            y1,
            x2 = d3Width - d3CellWidth - d3PaddingRight,
            y2,
            x3 = x2,
            y3,
            x4 = x1,
            y4;
        
        _.each( alignment.SegmentIDsInBaseText, function( segmentId ) {
          // get basetext segments for aligment basetext ids
          baseTextSegments.filter( function( basetextsegment, i ) {
            if ( basetextsegment.ID == segmentId ) {
              y1 = basetextsegment.pos.y;
              y4 = y1 + basetextsegment.size.h;
              return true;
            }
          });
        });

        _.each( alignment.SegmentIDsInVersion, function( segmentId ) {
          // get versiontext segments for aligment versiontext ids
          versionTextSegments.filter( function( versiontextsegment, i ) {
            if ( versiontextsegment.ID == segmentId ) {
              y2 = versiontextsegment.pos.y;
              y3 = y2 + versiontextsegment.size.h;
              return true;
            }
          });
        });

        return x1 + ',' + y1 + ' ' + x2 + ',' + y2 + ' ' + x3 + ',' + y3 + ' ' + x4 + ',' + y4;
      })
      .attr( 'data-alignmentid', function( d ) { return d.ID; } )
      .on( 'mouseover', highlightConnection )
      .on( 'mouseout', unhighlightAll );
  }
}

function highlightSegment( segment ) {
  d3.selectAll( 'rect' ).classed( 'active', false );
  d3.selectAll( 'polygon' ).classed( 'active', false );
  
  d3.select( this ).classed( 'active', true );
  $( 'span[data-eblasegid="' + segment.ID + '"]' ).addClass( 'active' );
  // first select all connections for this segment
  var matchedConnections = alignmentViz.selectAll( 'polygon' )
    .filter( function( conn, i ) {
      a = _.find( conn.SegmentIDsInBaseText, function( segmentId ) { return segmentId == segment.ID } );
      b = _.find( conn.SegmentIDsInVersion, function( segmentId ) { return segmentId == segment.ID } );
      return !_.isUndefined( a ) || !_.isUndefined( b );
    })
    .each( function( connection ) {
      highlightConnection.call( d3.select( '[data-alignmentid="' + connection.ID + '"]' )[0][0], connection );
    });
}

function highlightConnection( connection ) {
  d3.select( this ).classed( 'active', true );
  // do it with this == selected connection
  // highlight all elements these connections relate to
  d3.selectAll( 'rect' )
    .filter( function( seg, i ) {
      a = _.find( connection.SegmentIDsInBaseText, function( segmentIDInBaseText ) { return segmentIDInBaseText == seg.ID; } );
      b = _.find( connection.SegmentIDsInVersion, function( segmentIDInVersionText ) { return segmentIDInVersionText == seg.ID; } );
      return !_.isUndefined( a ) || !_.isUndefined( b );
    })
    .each( function( segment, i ) {
      $( 'span[data-eblasegid="' + segment.ID + '"]' ).addClass( 'active' );
    })
    .classed( 'active', true );
}

function unhighlightAll( target ) {
  d3.selectAll( 'rect' ).classed( 'active', false );
  d3.selectAll( 'polygon' ).classed( 'active', false );
  $( 'span' ).removeClass( 'active' );
}

function toggleSegmentSelect( segment ) {
  var el = d3.select( this );
  var selected = !el.classed( 'selected' );

  d3.selectAll( 'rect' ).classed( 'selected', false );
  d3.selectAll( 'polygon' ).classed( 'selected', false );
  $( 'span' ).removeClass( 'selected' );

  if ( selected ) {
    el.classed( 'selected', true );
    $( 'span[data-eblasegid="' + segment.ID + '"]' ).addClass( 'selected' );

    if ( el.attr( 'type' ) == 'basetext' ) {
      $( '#basetext' ).vvv().scrollTo( $( 'span[data-eblasegid="' + segment.ID + '"]' ) );
    }
    else {
      $( '#versiontext' ).vvv().scrollTo( $( 'span[data-eblasegid="' + segment.ID + '"]' ) );
    }

    // first select all connections for this segment
    var matchedConnections = alignmentViz.selectAll( 'polygon' )
      .filter( function( conn, i ) {
        a = _.find( conn.SegmentIDsInBaseText, function( segmentId ) { return segmentId == segment.ID } );
        b = _.find( conn.SegmentIDsInVersion, function( segmentId ) { return segmentId == segment.ID } );
        return !_.isUndefined( a ) || !_.isUndefined( b );
      })
      .each( function( connection ) {
        toggleConnectionSelect.call( d3.select( '[data-alignmentid="' + connection.ID + '"]' )[0][0], connection );
      });
  }
}

function toggleConnectionSelect( connection ) {
  d3.select( this ).classed( 'selected', true );
  // do it with this == selected connection
  // highlight all elements these connections relate to
  var firstBasetextSegment, firstVersiontextSegment;

  d3.selectAll( 'rect' )
    .filter( function( segment, i ) {
      a = _.find( connection.SegmentIDsInBaseText, function( segmentIDInBaseText ) { return segmentIDInBaseText == segment.ID; } );
      b = _.find( connection.SegmentIDsInVersion, function( segmentIDInVersionText ) { return segmentIDInVersionText == segment.ID; } );
      return !_.isUndefined( a ) || !_.isUndefined( b );
    })
    .sort( function( a, b ) {
      return a.startPos - b.startPos;
    })
    .classed( 'selected', true )
    .each( function ( segment ) {
      if ( d3.select( this ).attr( 'type' ) == 'basetext' && _.isUndefined( firstBasetextSegment ) ) {
        $( '#basetext' ).vvv().scrollTo( $( 'span[data-eblasegid="' + segment.ID + '"]' ) );
        firstBasetextSegment = segment;
      }
      else if ( _.isUndefined( firstVersiontextSegment ) ) {
        $( '#versiontext' ).vvv().scrollTo( $( 'span[data-eblasegid="' + segment.ID + '"]' ) );
        firstVersiontextSegment = segment;
      }

      $( 'span[data-eblasegid="' + segment.ID + '"]' ).addClass( 'selected' );
    });
}