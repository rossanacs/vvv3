/**
* (namespaced) VVV jQuery plugin according to:
* http://www.slideshare.net/dfleury/jquery-namespace-pattern
*/

(function ($) {
    /*
    * Google Translate API Key
    */
    var API_KEY = 'AIzaSyAmvGNlbYPF4RZzJAOcyGPu97e9p2rVEo0';

    var _useMSTrans = true;

    var parts, globalOptions = { CorpusName: _corpusName };

    $.fn.vvv = function (globalConfig) {
        $.extend(globalOptions, globalConfig);
        $.extend(this, parts);
        return this;
    }

    /*
    *   jQuery style plugin part
    *   callable on a selection & will use it’s information
    */
    parts = {
        scrollTo: function (element) {
            var $p = $(this).parent('.scroll-content');
            var centerOffset = $p.height() * 0.35;
            $p.animate({ scrollTop: $p.scrollTop() + element.offset().top - $p.offset().top - centerOffset }, { duration: 'slow', easing: 'swing' });
        },

        translate: function (from, to, callback) {
            var text = $.trim($(this)).text();
            if (text.length == 0) {
                callback('');
                return;
            }
            var q = 'https://www.googleapis.com/language/translate/v2?key=AIzaSyAmvGNlbYPF4RZzJAOcyGPu97e9p2rVEo0&source=' + from + '&target=' + to + '&q=' + text;
            $.getJSON(q + '&callback=?', function (response) {
                callback(response.data.translations[0].translatedText);
            });
        }
    };

    $.extend({

        vvv: {
            useMSTrans: function () {
                return _useMSTrans;
            },

            getSegments: function (args, callback, err) {
                var url = _siteUrl + 'Api/GetSegmentInformation';

                args.CorpusName = _.isUndefined(args.CorpusName) ? _corpusName : args.CorpusName;
                args.includeAttribs = _.isUndefined(args.includeAttribs) ? false : args.includeAttribs;

                $.getJSON(url, args, function (data) {
                    if (data.Succeeded) {
                        callback({ segments: data.SegmentDefinitions, documentLength: data.DocumentLength });
                    }
                    else {
                        $.vvv.showErrorMessage(data.ErrorMsg);
                        if (!_.isUndefined(err)) {
                            err(data);
                        }
                    }
                })
                .error(function (error) {
                    $.vvv.showErrorMessage(error.responseText);
                });
            },

            getText: function (args, callback, err) {
                var url = _siteUrl + 'Api/GetVersionText';

                args.CorpusName = _.isUndefined(args.CorpusName) ? _corpusName : args.CorpusName;

                $.getJSON(url, args, function (data) {
                    if (data.Succeeded) {
                        callback(data.Text);
                    }
                    else {
                        $.vvv.showErrorMessage(data.ErrorMsg);
                        if (!_.isUndefined(err)) {
                            err(data);
                        }
                    }
                })
                .error(function (error) {
                    $.vvv.showErrorMessage(error.responseText);
                });
            },

            getAlignments: function (args, callback, err) {
                var url = _siteUrl + "Aligner/GetAlignments";

                var args =
        {
            'BaseTextModel.CorpusName': _.isUndefined(args.BaseTextModel.CorpusName) ? _corpusName : args.BaseTextModel.CorpusName,
            'BaseTextModel.ExtractSegmentID': _.isUndefined(args.BaseTextModel.ExtractSegmentID) ? -1 : args.BaseTextModel.ExtractSegmentID,
            'VersionModel.CorpusName': _.isUndefined(args.VersionModel.CorpusName) ? _corpusName : args.VersionModel.CorpusName,
            'VersionModel.NameIfVersion': _.isUndefined(args.VersionModel.VersionName) ? null : args.VersionModel.VersionName,
            'VersionModel.ExtractSegmentID': _.isUndefined(args.VersionModel.ExtractSegmentID) ? -1 : args.VersionModel.ExtractSegmentID
        };

                $.ajax({ url: url, type: 'POST', data: args, dataType: 'json', async: true, traditional: true,
                    complete: function (data, textStatus) {
                        if (textStatus != 'success') {
                            alert('The call to GetAlignments failed.');
                            // TODO - more info and/or error log.
                            return;
                        }
                        var result = JSON.parse(data.responseText);
                        if (result.Succeeded) {
                            callback({ data: result /* JSON.parse(data.responseText)*/, status: data.status });
                        }
                        else {
                            err({ msg: result.ErrorMsg });
                        }

                    }
                });
            },

            getSegmentVariationData: function (args, callback, err) {
                var url = _siteUrl + "Api/GetSegmentVariation";

                args.CorpusName = _.isUndefined(args.CorpusName) ? _corpusName : args.CorpusName;

                $.getJSON(url, args, function (data) {
                    if (data.Succeeded) {
                        if (!data.SegmentVariationData) {
                            $.vvv.showErrorMessage('Variation data null for segment ID ' + args.SegmentID);
                        }
                        else
                            callback(data.SegmentVariationData);
                    }
                    else {
                        $.vvv.showErrorMessage(data.ErrorMsg);
                        if (!_.isUndefined(err)) {
                            err(data);
                        }
                    }
                })
                .error(function (error) {
                    $.vvv.showErrorMessage(error.responseText);
                });

                /*$.ajax({ url: url, type: 'GET', data: args, dataType: 'json', traditional: true, cache: false,
                complete: function ( data, textStatus )
                {
                console.log( data );
                console.log( textStatus );
                var dataText;
                if ( data.status == 200 )
                {
                // quick hack to change number format from GER locale to US (.)
                var numberFormatResponse = data.responseText.replace( /((\d+),(\d+))/g, '$2.$3' );
                dataText = JSON.parse( numberFormatResponse );
                }
                else
                {
                dataText = "no result";
                }

                if ( sort.sortBy == 'name' ) {
                dataText.VersionSegmentVariations = _.sortBy( dataText.VersionSegmentVariations, 'VersionName' );
                }
                else if ( sort.sortBy == 'length' ) {
                dataText.VersionSegmentVariations = _.sortBy( dataText.VersionSegmentVariations, function( d ) { return d.VersionText.length } );
                }
                else if ( sort.sortBy == 'eddy' ) {
                dataText.VersionSegmentVariations = _.sortBy( dataText.VersionSegmentVariations, 'EddyValue' );
                }
                else if ( sort.sortBy == 'year' ) {
                dataText.VersionSegmentVariations = _.sortBy( dataText.VersionSegmentVariations, 'ReferenceDate' );
                }

                if ( sort.sortAscending == false ) {
                dataText.VersionSegmentVariations = dataText.VersionSegmentVariations.reverse();
                }
                        
                callback( { data: dataText, status: data.status } );
                }
                });*/
            },

            getTranslation: function (text, from, to, callback) {

                if (_useMSTrans) {
                    var url = _siteUrl + 'Api/GetMachineTranslation';

                    var args = {};

                    args.CorpusName = _.isUndefined(args.CorpusName) ? _corpusName : args.CorpusName;
                    args.sourceLangCode = from;
                    args.targetLangCode = to;
                    args.text = text;

                    $.getJSON(url, args, function (data) {
                        if (data.Succeeded) {
                            callback(data.Text);
                        }
                        else {
                            $.vvv.showErrorMessage(data.ErrorMsg);
                            //                            if (!_.isUndefined(err)) {
                            //                                err(data);
                            //                            }
                        }
                    })
                    .error(function (error) {
                        $.vvv.showErrorMessage(error.responseText);
                    });

                }
                else {
                    var q = 'https://www.googleapis.com/language/translate/v2?key=AIzaSyAmvGNlbYPF4RZzJAOcyGPu97e9p2rVEo0&source=' + from + '&target=' + to + '&q=' + text;
                    $.getJSON(q + '&callback=?', function (response) {
                        if (response)
                            if (response.data)
                                if (response.data.translations)
                                    if (response.data.translations.length)

                                        callback(response.data.translations[0].translatedText);
                    });

                }



            },

            showErrorMessage: function (msg) {
                // TODO:
                alert('Oh Snap! Something went wrong. ' + msg);
            }
        }
    });
})(jQuery);