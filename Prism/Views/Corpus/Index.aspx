<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.CorpusModel>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%= Url.Content("~/Scripts/vvv/jquery.isotope.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/corpus.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/Prism.js") %>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - Corpus:
    <%: Model.CorpusName%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.HiddenFor(model => model.CorpusName, new { id = "corpusname" })%>

    <div class="container">
        <div class="subnav subnav-fixed">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="pull-right">
                            <% if (Model.CanWrite)
                               { %>
                            <div class="btn-group">
                                <a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#">Edit Corpus
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <%: Html.ActionLink("Add new version", "CreateVersion", new { CorpusName = Model.CorpusName }, new { @class = "" })%></li>
                                    <li>
                                        <%: Html.ActionLink("Corpus Admin", "Admin", "Corpus", new { CorpusName = Model.CorpusName }, new { @class = "" })%></li>
                                </ul>
                            </div>
                            <%} %>
                        </div>
                        <h1>
                            <%: Model.CorpusName%>
                            <small>
                                <%: Model.Description %></small></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="corpus-item base-text span12">
                        <div class="row">
                            <div class="span7">
                                <h3 class="version-label">
                                    Base Text <small> 
                                        <% if (Model.BaseText.ReferenceDate.HasValue) { %>
                                     (<%: Model.BaseText.ReferenceDate %>)
                                     <% } %>
                                     <%: Model.Cultures.FirstOrDefault(p => p.Value == Model.LanguageCode).Key %>
                                     </small></h3>
                                <div class="description">
                                    <p>
                                        <%: Model.BaseText.Description %></p>
                                </div>
                            </div>
                            <div class="span5">
                                <div class="admin pull-right">
                                    <% if (Model.CanWrite)
                                       { %>
                                    <div class="btn-group pull-right">
                                        <a class="btn btn-mini dropdown-toggle btn-primary" data-toggle="dropdown" href="#">
                                            Work <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <%: Html.ActionLink("View", "Index", "Document", new { CorpusName = Model.CorpusName }, new { @class = "" })%></li>
                                            <li>
                                                <%: Html.ActionLink("Upload", "Upload", "Document", new { CorpusName = Model.CorpusName }, new { @class = "" })%></li>
                                            <li>
                                                <%: Html.ActionLink("Details", "DocumentMetadata", "Corpus", new { CorpusName = Model.CorpusName }, new { @class = "" })%></li>
                                            <li>
                                                <%: Html.ActionLink("Export", "ExportDocument", "Document", new { CorpusName = Model.CorpusName}, new { @class="" })%></li>
                                        </ul>
                                    </div>
                                    
                                    <% } %>
                                    <div class="btn-group pull-right">
                                        <a class="btn btn-mini dropdown-toggle btn-primary" data-toggle="dropdown" href="#">
                                            Charts <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <%: Html.ActionLink("Variation", "EddyOverviewChart", "Charts", new { CorpusName = Model.CorpusName }, new { @class = "", @title = "Visually compare Eddy values across the text" })%></li>
                                            <li>
                                                <%: Html.ActionLink("History", "EddyHistory", "Charts", new { CorpusName = Model.CorpusName, BaseTextSegmentId = -1 }, new { @class = "", @title = "Plot Eddy values versus reference date" })%></li>
                                            <li>
                                                <%: Html.ActionLink("Distribution", "VivDistributionChart", "Charts", new { CorpusName = Model.CorpusName }, new { @class = "", @title="View distribution of Viv values" })%></li>
                                            <li>
                                                <%: Html.ActionLink("Viv vs. segment length", "VivVersusSegmentLengthChart", "Charts", new { CorpusName = Model.CorpusName }, new { @class = "" })%></li>
                                        </ul>
                                    </div>
                                    <a href="<%: Url.Action( "SegmentVersionsTable", "Visualise", new { CorpusName = Model.CorpusName } ) %>"
                                        rel="tooltip" data-placement="top" title="Tabular display of selected segments and their translations"
                                        class="btn btn-mini pull-right tt">Table</a> 
                                    <a href="<%: Url.Action( "Alignments", "Visualise", new { CorpusName = Model.CorpusName } ) %>"
                                        rel="tooltip" data-placement="top" title="<i>Alignment Maps</i>: overviews of deletions and re-ordering"
                                        class="btn btn-mini pull-right tt">Alignments</a> 
<%--                                    <a href="<%: Url.Action( "EddyOverviewChart", "Charts", new { CorpusName = Model.CorpusName } ) %>"
                                            rel="tooltip" data-placement="top" title="Visually compare Eddy values across the text"
                                            class="btn btn-mini pull-right tt">Variation</a>--%>
<%--                                    <a href="<%: Url.Action( "VivVersusSegmentLengthChart", "Charts", new { CorpusName = Model.CorpusName } ) %>"
                                            rel="tooltip" data-placement="top" title="test"
                                            class="btn btn-mini pull-right tt">test</a>--%>
                                    <% if (Model.CorpusName == "Othello, Act 1 Scene 3")
                                       { %>
                                    <a href="<%: Url.Action( "OthelloMap", "Visualise", new { CorpusName = Model.CorpusName } ) %>"
                                        rel="tooltip" data-placement="top" title="An interactive map showing German translations from the Othello corpus"
                                        class="btn btn-mini pull-right tt">Map</a>
                                    <% } %>
<%--                                    <a href="<%: Url.Action( "EddyHistory", "Charts", new { CorpusName = Model.CorpusName, BaseTextSegmentId = -1 } ) %>"
                                        rel="tooltip" data-placement="top" title="Plot Eddy values versus reference date"
                                        class="btn btn-mini pull-right tt">History</a> --%>
                                    <a href="<%: Url.Action( "Viv", "Visualise", new { CorpusName = Model.CorpusName } ) %>"
                                            rel="tooltip" data-placement="top" title="<i>Eddy and Viv</i>: see and sort all translations of a segment, with back-translations"
                                            class="btn btn-mini pull-right tt">E &amp; V</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="sorting span12">
                        <div class="pull-right subnav-item-right">
                            <div id="sort-direction" class="option-set" data-option-key="sortAscending">
                                <div class="btn-group pull-right">
                                    <a href="#sortAscending=true" data-option-value="true" class="btn btn-mini optionlink disabled">
                                        Asc</a> <a href="#sortAscending=false" data-option-value="false" class="btn btn-mini optionlink">
                                            Desc</a>
                                </div>
                                <div class="headline pull-right">
                                    ORDER</div>
                            </div>
                        </div>
                        <div class="pull-right subnav-item-right">
                            <div id="sort-by" class="option-set" data-option-key="sortBy">
                                <form class="form-horizontal viv-sort">
                                <div class="control-label">
                                    SORT BY</div>
                                <div class="controls">
                                    <select id="corpus-sort-select">
                                        <option data-option-value="year">Year</option>
                                        <option data-option-value="author">Author</option>
                                    </select>
                                </div>
                                </form>
                            </div>
                        </div>
                        <div>
                            <div class="headline">
                                &darr;
                                <%: Model.VersionList.Length %>
                                VERSIONS</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="corpus-index-content-wrapper" class="container">
            <div class="corpus-item">
            <span class="btn btn-mini tt " title="Add all versions to the selection for this corpus" id="corp-all-selection" >All</span>
            <span class="btn btn-mini tt " title="Remove all versions from the selection for this corpus" id="corp-none-selection" >None</span>
<%--            <div class="admin pull-right">
            </div>
--%>            </div>
                        <div id="corpus-index-content">

                <%  if (Model.VersionList != null)
                    {
                        var selectedVersions = Prism.Controllers.CorpusController.GetActiveCorpusSelection();
                        foreach (var item in Model.VersionList)
                        {
                            bool inSelection = selectedVersions.IndexOf(item.NameIfVersion) != -1;
                            string stylestringadd = "style = 'display: " + (!inSelection ? "inline" : "none") + "'";
                            string stylestringrem = "style = 'display: " + (!inSelection ? "none" : "inline") + "'";
                            string styleitem = inSelection ? string.Empty : "style='color: gray'";
                            %>
                <div class="corpus-item version corp-selection-item" >
                    <div class="admin pull-right">
                        <span class="btn btn-mini tt corp-add-selection" 
                            <%=stylestringadd %>
                            data-id="<%: "veradd-" + item.NameIfVersion %>"
                            title="Add this version to the selection for this corpus" data-placement="left">
                            +</span> 
                         <span class="btn btn-mini tt corp-rem-selection" 
                            <%=stylestringrem %>
                            data-id="<%: "verrem-" + item.NameIfVersion %>"
                                title="Remove this version from the selection for this corpus" data-placement="left">
                                -</span> <a href="<%: Url.Action("Info", "Document", new { CorpusName = Model.CorpusName, NameIfVersion = item.NameIfVersion } ) %>"
                                    class="btn btn-mini tt" title="Learn more about this version" data-placement="left">
                                    Details</a>
                        <% if (Model.CanWrite)
                           { %>
                        <a href="#" class="btn btn-mini btn-primary po-admin" rel="popover" title="Work">Work</a>
                        <div class="admin-menu" style="display: none">
                            <div class="admin-buttons-wrapper">
                                <%: Html.ActionLink("View", "Index", "Document", new { CorpusName = Model.CorpusName, NameIfVersion = item.NameIfVersion }, new { @class = "btn btn-mini" })%>
                                <%: Html.ActionLink("Edit Details", "DocumentMetadata", "Corpus", new { CorpusName = Model.CorpusName, NameIfVersion = item.NameIfVersion }, new { @class = "btn btn-mini" })%>
                                <%: Html.ActionLink("Upload", "Upload", "Document", new { CorpusName = Model.CorpusName, NameIfVersion = item.NameIfVersion }, new { @class = "btn btn-mini" })%>
                                <%: Html.ActionLink("Align", "Index", "Aligner", new { CorpusName = Model.CorpusName, VersionName = item.NameIfVersion }, new { @class = "btn btn-mini" })%>
                                <%: Html.ActionLink("Export", "ExportDocument", "Document", new { CorpusName = Model.CorpusName, VersionName = item.NameIfVersion }, new { @class = "btn btn-mini" })%>
                                <a class="btn btn-mini" onclick="return confirm('Are you sure you wish to delete this version?');"
                                href="<%: Url.Action( "DeleteVersion", "Corpus", new { CorpusName = Model.CorpusName, VersionName = item.NameIfVersion } ) %>">
                                    <i class="icon-trash"></i></a>
                            </div>
                        </div>
                        <%  }%>
                    </div>
                    <h3 class="version-label" >
                        <a href="<%: Url.Action( "Index", "Visualise", new {CorpusName = Model.CorpusName, NameIfVersion = item.NameIfVersion} ) %>"
                            class="version-title-link tt" title="Click to access Parallel View" data-placement="right" 
                            <%= styleitem %>
                            data-id="<%: "verlink-" + item.NameIfVersion %>" data-insel="<%: inSelection ? "1" : "0" %>">
                            <%: item.NameIfVersion%>
                            <span class="year">
                            <% if (item.ReferenceDate.HasValue)
                               { %>
                            (<%: item.ReferenceDate%>)
                            <% } %>
                            </span> <small>
                                <%: Model.Cultures.FirstOrDefault(p => p.Value == item.LanguageCode).Key%>
                                <% if (!string.IsNullOrEmpty(item.Genre)) { %>
                                , <%: item.Genre%>
                                <% } %>
                                <% if (!string.IsNullOrEmpty(item.CopyrightInfo)) { %>
                                , <%: item.CopyrightInfo%>
                                <% } %>
                                </small> </a>
                    </h3>
                </div>
                <%      }
                    }
                %>
            </div>
        </div>
        <div>
            <%: Html.ActionLink("Back to List", "Index", "CorpusStore", null, new { @class="btn" } ) %>
        </div>
    </div>
</asp:Content>
