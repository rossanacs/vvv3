<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.CorpusModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	    VVV - Corpus:
    <%: Model.CorpusName%> - Admin
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">
		<div class="page-header">
			<% if (Model.CanWrite)
               { %>
<%--            <div class="corpus-options pull-right">
                <%: Html.ActionLink("Set up predefined segment attributes", "PredefinedSegmentAttributes", "Corpus", new { CorpusName = Model.CorpusName, NameIfVersion = string.Empty }, new { @class = "btn btn-mini" })%>
            </div>
--%><%--            <div class="corpus-options pull-right">
                <%: Html.ActionLink("Export", "ExportCorpus", "Corpus", new { CorpusName = Model.CorpusName }, new { @class = "btn btn-mini" })%>
            </div>
--%><%--            <div class="corpus-options pull-right">
                <%: Html.ActionLink("Export (plain)", "ExportCorpusPlain", "Corpus", new { CorpusName = Model.CorpusName }, new { @class = "btn btn-mini" })%>
            </div>--%>
            <%} %>
			<h1><%: Model.CorpusName%> - Admin</h1>
		</div>

       <%-- <div class="row">
        	<div class="span12">
	            Background calculation status: <span id="calcstatus"><%: ViewData["calcstatus"]%></span><br />
	            <input type="hidden" id="corpusbusy" value="<%: ViewData["corpusbusy"] %>" />       
	            <select id="variation-type-select">
	            </select>
	            <input type="button" id="startcalc" value="Start" />
	            <input type="button" id="stopcalc" value="Stop" />
			</div>
        </div>--%>
        <div class="row">
        <fieldset>
        <legend>Segment attributes</legend>
<%: Html.ActionLink("Set up predefined segment attributes", "PredefinedSegmentAttributes", "Corpus", new { CorpusName = Model.CorpusName, NameIfVersion = string.Empty }, new { @class = "btn btn-mini" })%>
        </fieldset>
        </div>
        <div class="row">
        <fieldset>
        <legend>Tokenisation</legend>
         <input type="button" id="retok" value="Re-tokenise" />
        </fieldset>
        </div>
        <div class="row">
        <fieldset>
        <legend>Export corpus</legend>
        (Note: this will only export versions that are currently 'selected'. Hover the mouse pointer over the 'CORPUS' menu item above to see which are.)
        <%: Html.ActionLink("Export", "ExportCorpus", "Corpus", new { CorpusName = Model.CorpusName }, new { @class = "btn btn-mini" })%><br />
<%: Html.ActionLink("Export (plain)", "ExportCorpusPlain", "Corpus", new { CorpusName = Model.CorpusName }, new { @class = "btn btn-mini" })%>
        </fieldset>
        </div>
        <div class="row">
        <% using (Html.BeginForm("Import", "Corpus", FormMethod.Post, new { enctype = "multipart/form-data" }))
           {%>
        <%: Html.HiddenFor(model => model.CorpusName, new { id = "uploadcorpusname" })%>
        <fieldset>
            <legend>Import corpus</legend>
        <%: Html.ValidationSummary(true) %>
            <label for="file">
                Filename:</label>
            <input type="file" name="file" id="file" />
<%--            <%: Html.EnumDropDownListFor(model => model.encoding) %>--%>
            <p>
                <input type="submit" value="Import" />
            </p>
        </fieldset>
        <% } %>

        </div>
    </div>
    <%: Html.HiddenFor(x => x.CorpusName, new { id = "corpusname" })%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
	<script src="<%= Url.Content("~/Scripts/Prism-Utils.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/Prism-CorpusAdmin.js") %>" type="text/javascript"></script>

</asp:Content>
