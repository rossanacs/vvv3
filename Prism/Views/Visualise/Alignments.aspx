<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.CorpusModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV -
    <%: ViewData["corpusname"] %>
    - Alignment View
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	
	<div class="subnav subnav-fixed">
        <div class="container">
        	<div class="row title">
        		<div class="span12">
        			<h1><%: ViewData["corpusname"] %> <small>Alignment View</small></h1>
        		</div>
        	</div>
	        <div class="row">
	            <div class="span6">
	                <h3><a href="#" class="tt" title="The Base Text is a purpose-specific version of the translated work." rel="tooltip" data-placement="right">Base Text <small><%: ViewData["basetextdate"] %></small></a></h3>
	            </div>
	            <div id="versionlist" class="span6">
	                <form class="form-horizontal pull-right">
	                <fieldset>
	                    <div class="control-group">
	                        <label class="control-label" for="versionlist">
	                            Version</label>
	                        <div class="controls">
	                            <% Response.Write(ViewData["versionlist"].ToString()); %>
	                        </div>
	                    </div>
	                </fieldset>
	                </form>
	            </div>
	        </div>
        </div>
    </div>
	
    <div class="container alignments withSubnav">
        <div class="row">
        	<div class="scroll-wrapper span4" style="height:400px;">
        		<div class="row scroll-content">
	                <div id="basetext" class="text no-select">
        				<% Response.Write(ViewData["basetextcontent"].ToString()); %>
        			</div>
	            </div>
            </div>
        	<div class="scroll-wrapper span4" style="height:400px;">
        		<div class="row scroll-content">
	            	<div id="alignmentviz"></div>
	            </div>
            </div>
            <div class="scroll-wrapper span4" style="height:400px;">
        		<div class="row scroll-content">
	                <div id="versiontext" class="text no-select">
        			</div>
	            </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="corpusname" value="<%=ViewData["corpusname"] %>" />
    <input type="hidden" id="selectedsegid" value="-1" />
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%= Url.Content("~/Scripts/Prism-Formatting.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/Prism-Utils.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/d3/d3.v2.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/kinetic.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/chroma/chroma.min.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/softdivscroll.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/jquery.waypoints.min.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/vvv/alignments/alignments.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("http://use.typekit.com/oiq3xqg.js") %>" type="text/javascript"></script>
    <script type="text/javascript">        try { Typekit.load(); } catch (e) { }</script>
</asp:Content>

<asp:Content ID="Help" ContentPlaceHolderID="HelpMenuItem" runat="server">
<a id="help-popover" href="#"><i class="icon-question-sign icon-white"></i> What&#8217;s this?</a>
<div id="help-content" style="display:none">
	<p>
	Each bar represents a segment. In Othello texts, here, each segment is a speech. Its thickness represents the word-count. Select a translation from the drop-down list, and see how some translators cut, expand, or re-order the original work. Try B&auml;rfuß, Felsenstein or Zimmer, for example. Explore the texts themselves in the <i>Fruit Machine</i> view.
	</p>
</div>
</asp:Content>
