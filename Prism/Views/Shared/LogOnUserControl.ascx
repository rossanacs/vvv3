﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="btn-group pull-right">
	<%
    	if (Request.IsAuthenticated) {
	%>
	<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
		<i class="icon-user icon-white"></i> <%: Page.User.Identity.Name %>
		<span class="caret"></span>
	</a>
	<ul class="dropdown-menu"> 
		<li><a href="<%= Url.Action("LogOff", "Account") %>">Sign Out</a></li>
        <%
            if (Prism.Controllers.AccountController.CanChangePassword)
            {
             %>
		<li><a href="<%= Url.Action("ChangePassword", "CorpusStore") %>">Change password</a></li>
        <% } %>
	</ul>
	<%
    	}
    	else {
    %>
	<a href="<%= Url.Action("LogOn", "Account") %>" class="btn">Sign In</a>
	<%
		}
	%>
</div>