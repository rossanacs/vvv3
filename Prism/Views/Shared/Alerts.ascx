﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Prism.Models" %>
<%var alerts = TempData.ContainsKey(Alert.TempDataKey)
                    ? (List<Alert>)TempData[Alert.TempDataKey]
                    : new List<Alert>();
    //var newAlert = new Alert() {AlertStyle=AlertStyles.Success, Dismissable = true, Message = "Testing message."};
    //alerts.Add(newAlert);
    if (alerts.Any())%>
<%{%>
<%--    <hr />--%>
<%}%>
<%foreach (var alert in alerts)
{
    var dismissableClass = alert.Dismissable ? "alert-dismissable" : null;%>
    <div class="alert alert-<%:alert.AlertStyle%> <%:dismissableClass%>">
        <% if (alert.Dismissable)
        {%>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <%}%>
        <%=MvcHtmlString.Create(alert.Message)%>
    </div>
<%}%>

