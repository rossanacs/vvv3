﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.HtmlUploadModel>" %>

<%@ Import Namespace="Prism.HtmlHelpers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - upload document
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%= Url.Content("~/ckeditor/ckeditor.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/ckeditor/adapters/jquery.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/Prism-Upload.js") %>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="page-header">
            <h2>
                Upload
                <%: Model.CorpusName %>:
                <% if (string.IsNullOrEmpty(Model.NameIfVersion))
                   { %>
                base text
                <% }
                   else
                   { %>
                version '<%:Model.NameIfVersion %>'<% } %>
            </h2>
            <p>
                NOTE: Uploading a document will delete any existing segmentation and alignment information
                relating to the document.</p>
            <p>
                If your text is in a Word document or similar, the easiest way to upload it is to
                open the document, select all of it, copy the selection to the clipboard, then paste
                it into the editing box below. Alternatively, if your text is in an HTML document,
                you can select the file for upload directly. (With a Word document, you <i>could</i>
                use 'Save As' to convert it to HTML ... but that tends to produce results that are
                very messy internally.)</p>
            <fieldset>
                <legend>Paste content</legend>
                <div id="editorenclosurediv">
                    <textarea id="doceditor" name="doceditor">       </textarea>
                </div>
                <input type="button" class="btn-primary" value="Upload content" id="uploadcontent" />
            </fieldset>
        </div>
        <h2><%: Html.ValidationSummary(true) %></h2>
        <% using (Html.BeginForm(null, null, FormMethod.Post, new { enctype = "multipart/form-data" }))
           {%>
        <%: Html.HiddenFor(model => model.CorpusName, new { id = "uploadcorpusname" })%>
        <%: Html.HiddenFor(model => model.NameIfVersion, new { id = "uploadnameifversion" })%>
        <fieldset>
            <legend>Upload file</legend>
            <label for="file">
                Filename:</label>
            <input type="file" name="file" id="file" />
            <%: Html.EnumDropDownListFor(model => model.encoding) %>
            <p> <input type="submit" class="btn-primary" value="Upload file" /> </p>
        </fieldset>
        <% } %>
        <% using (Html.BeginForm("Update", "Document", FormMethod.Post, new { enctype = "multipart/form-data" }))
           {%>
        <%: Html.HiddenFor(model => model.CorpusName, new { id = "updatecorpusname" })%>
        <%: Html.HiddenFor(model => model.NameIfVersion, new { id = "updatenameifversion" })%>
        <fieldset>
            <legend>Update file</legend>
            <label for="file">Filename:</label>
            <input type="file" name="file" id="file1" />
            <p><input type="submit" class="btn-primary" value="Update file" /> </p>
        </fieldset>
        <% } %>
        
        <div>
            <%: Html.ActionLink("Back to Corpus", "Index", "Corpus", new { CorpusName = Model.CorpusName }, new { @class="btn" })%>
            <%--<%: Html.ActionLink("Back to Corpus", "Index",new { CorpusName = Model.CorpusName, NameIfVersion = Model.NameIfVersion }, new { @class="btn" } ) %>--%>
        </div>
    </div>
</asp:Content>
