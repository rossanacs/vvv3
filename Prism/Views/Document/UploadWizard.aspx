﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/New.Master" Inherits="System.Web.Mvc.ViewPage<Prism.Models.UploadFilesResult>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - Wizard
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">  

    <!-- CSS -->
    <link href="<%= Url.Content("http://fonts.googleapis.com/css?family=Roboto:400,100,300,500") %>" rel="stylesheet" type="text/css" />
    <link href="<%= Url.Content("~/Content/newer/css/bootstrap-3-3.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= Url.Content("~/Content/newer/css/bootstrap-3-3.min.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= Url.Content("~/Content/newer/css/font-awesome.min.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= Url.Content("~/Content/newer/css/form-elements.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= Url.Content("~/Content/newer/css/stylewizard.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= Url.Content("~/Content/newer/css/jquery.fileupload.css") %>" rel="stylesheet" type="text/css" />
    <!-- Javascript -->
    <script type="text/javascript">
        var $$ = jQuery;
    </script>
    <script src="<%= Url.Content("~/Scripts/newer/jquery-1.9.1.min.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/newer/bootstrap-3-3.min.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/newer/jquery.backstretch.min.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/newer/retina-1.1.0.min.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/newer/jquery-1.11.1.min.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/newer/scripts.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/newer/jquery.ui.widget.js") %>" type="text/javascript"></script>
    <script src="<%= Url.Content("~/Scripts/newer/jquery.fileupload.js") %>" type="text/javascript"></script>
    
    <script type="text/javascript">
        
        jQuery.noConflict();

        //$$(document).ready(function () {
        //    $$('#fileupload').fileupload({
        //        dataType: 'json',
        //        url: '/Document/UploadFiles',
        //        autoUpload: true,
        //        done: function (e, data) {
        //            $$('.file_name').html(data.result.name);
        //            $$('.file_type').html(data.result.type);
        //            $$('.file_size').html(data.result.size);
        //        }
        //    }).on('fileuploadprogressall', function (e, data) {
        //        var progress = parseInt(data.loaded / data.total * 100, 10);
        //        $$('.progress .progress-bar').css('width', progress + '%');
        //    });
        //});
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">       
        <!-- Top menu -->
		<%--<nav class="navbar navbar-inverse navbar-no-bg" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="UploadWizard.aspx">Bootstrap Wizard Tutorial - New Step</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<span class="li-text">
								Put some text or
							</span> 
							<a href="#"><strong>links</strong></a> 
							<span class="li-text">
								here, or some icons: 
							</span> 
							<span class="li-social">
								<a href="https://www.facebook.com/pages/Azmindcom/196582707093191" target="_blank"><i class="fa fa-facebook"></i></a> 
								<a href="https://twitter.com/anli_zaimi" target="_blank"><i class="fa fa-twitter"></i></a> 
								<a href="https://plus.google.com/+AnliZaimi_azmind" target="_blank"><i class="fa fa-google-plus"></i></a> 
								<a href="https://github.com/AZMIND" target="_blank"><i class="fa fa-github"></i></a>
							</span>
						</li>
					</ul>
				</div>
			</div>
		</nav>--%>

        <!-- Top content -->
        <div class="top-content">
            <div class="container">
                
                <%--<div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text">
                        <h1>Bootstrap <strong>Wizard</strong> Tutorial - New Step</h1>
                        <div class="description">
                       	    <p>
                                Learn how to add a new step to this free Bootstrap form wizard on 
                                <a href="http://azmind.com"><strong>AZMIND</strong></a>. You can also download the template with all the source files!
                            </p>
                        </div>
                    </div>
                </div>--%>
                
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 form-box">
                    	<form role="form" action="" method="post" class="f1">

                    		<h3>VVV Alignments</h3>
                    		<p>Follow the steps to add your files</p>
                    		<div class="f1-steps">
                    			<div class="f1-progress">
                    			    <div class="f1-progress-line" data-now-value="12.5" data-number-of-steps="4" style="width: 12.5%;"></div>
                    			</div>
                    			<div class="f1-step active">
                    				<div class="f1-step-icon"><i class="fa fa-user"></i></div>
                    				<p>base text</p>
                    			</div>
                    			<div class="f1-step">
                    				<div class="f1-step-icon"><i class="fa fa-key"></i></div>
                    				<p>segment</p>
                    			</div>
                    			<div class="f1-step">
                    				<div class="f1-step-icon"><i class="fa fa-question"></i></div>
                    				<p>version(s)</p>
                    			</div>
                    		    <div class="f1-step">
                    				<div class="f1-step-icon"><i class="fa fa-twitter"></i></div>
                    				<p>segment</p>
                    			</div>
                    		</div>
                    		
                    		<fieldset>
                    		    <h4>Tell us who you are:</h4>
                    			<div class="form-group">
                    			    <label class="sr-only" for="f1-first-name">First name</label>
                                    <input type="text" name="f1-first-name" placeholder="First name..." class="f1-first-name form-control" id="f1-first-name">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-last-name">Last name</label>
                                    <input type="text" name="f1-last-name" placeholder="Last name..." class="f1-last-name form-control" id="f1-last-name">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-about-yourself">About yourself</label>
                                    <textarea name="f1-about-yourself" placeholder="About yourself..." 
                                    	                 class="f1-about-yourself form-control" id="f1-about-yourself"></textarea>
                                </div>
                                <div class="f1-buttons">
                                    <button type="button" class="btn btn-next">Next</button>
                                </div>
                            </fieldset>

                            <fieldset>
                                <h4>Set up your account:</h4>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-email">Email</label>
                                    <input type="text" name="f1-email" placeholder="Email..." class="f1-email form-control" id="f1-email">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-password">Password</label>
                                    <input type="password" name="f1-password" placeholder="Password..." class="f1-password form-control" id="f1-password">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-repeat-password">Repeat password</label>
                                    <input type="password" name="f1-repeat-password" placeholder="Repeat password..." 
                                                           class="f1-repeat-password form-control" id="f1-repeat-password">
                                </div>
                                <div class="f1-buttons">
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="button" class="btn btn-next">Next</button>
                                </div>
                            </fieldset>
                            
                            <fieldset>
                                <h4>Security question:</h4>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-question">Question</label>
                                    <input type="text" name="f1-question" placeholder="Question..." class="f1-question form-control" id="f1-question">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-answer">Answer</label>
                                    <input type="text" name="f1-answer" placeholder="Answer..." class="f1-answer form-control" id="f1-answer">
                                </div>
                                <div class="f1-buttons">
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="button" class="btn btn-next">Next</button>
                                </div>
                            </fieldset>

                            <fieldset>
                                <h4>Social media profiles:</h4>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-facebook">Facebook</label>
                                    <input type="text" name="f1-facebook" placeholder="Facebook..." class="f1-facebook form-control" id="f1-facebook">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-twitter">Twitter</label>
                                    <input type="text" name="f1-twitter" placeholder="Twitter..." class="f1-twitter form-control" id="f1-twitter">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-google-plus">Google plus</label>
                                    <input type="text" name="f1-google-plus" placeholder="Google plus..." class="f1-google-plus form-control" id="f1-google-plus">
                                </div>
                                <div class="f1-buttons">
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="submit" class="btn btn-submit">Submit</button>
                                </div>
                            </fieldset>
                    	
                    	</form>
                    </div>
                </div>
                    
            </div>
        </div>
</asp:Content>
