using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using EblaAPI;
using Prism.Models;
using System.Net;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Threading;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.ServiceModel;

namespace Prism.Controllers
{
	public class ApiController : PrismController
	{
        static object _mstranslock = new object();
        static AdmAuthentication _admAuth;

        const string _clientid = "kftrans";
        
        static MSTranslator.LanguageServiceClient _client;

        static bool? _mstransAvailable = null;

        [HttpGet]
        public ActionResult GetMachineTranslation(string sourceLangCode, string targetLangCode, string text)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(text))
                {
                    var o = new { Succeeded = true, Text = string.Empty };
                    return MonoWorkaroundJson(o);
                }
                text = text.Trim();

                if (_mstransAvailable == null)
                {
                    lock (_mstranslock)
                    {
                        if (_mstransAvailable == null)
                        {
                            string secret = System.Configuration.ConfigurationManager.AppSettings["MSTransSecret"];

                            if (string.IsNullOrEmpty(secret))
                                _mstransAvailable = false;
                            else
                            {
                                _admAuth = new AdmAuthentication(_clientid, secret);
                                _client = new MSTranslator.LanguageServiceClient();
                                _mstransAvailable = true;
                                //System.Threading.Thread.Sleep(2000);
                            }
                        }
                    }

                } //  if (admauth null)

                if (!(_mstransAvailable.Value ))
                {
                    var o = new { Succeeded = true, Text = string.Empty };

                    return MonoWorkaroundJson(o);
                }

                var admToken = _admAuth.GetAccessToken();
                System.Diagnostics.Debug.Assert(_client != null);

                string authToken = "Bearer " + admToken.access_token;

                //Set Authorization header before sending the request
                HttpRequestMessageProperty httpRequestProperty = new HttpRequestMessageProperty();
                httpRequestProperty.Method = "POST";
                httpRequestProperty.Headers.Add("Authorization", authToken);

                // Creates a block within which an OperationContext object is in scope.
                using (OperationContextScope scope = new OperationContextScope(_client.InnerChannel))
                {
                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;
                    var options = new MSTranslator.TranslateOptions();
                    //////Set options if required
                    options.Category = "general";
                    options.ContentType = "text/plain";
                    options.User = "TestUserId";
                    options.Uri = "";
                    //Keep appId parameter blank as we are sending access token in authorization header.

                    var translations = _client.GetTranslations("", text,
                        sourceLangCode,
                        targetLangCode, 5, options);
                    //Console.WriteLine(string.Format("Available translations for source text '{0}' are", text));

                    string translatedText = string.Empty;
                    foreach (var translationMatch in translations.Translations)
                    {
                        // API actually never returns more than 1, and even if it did, we
                        // only want 1 for the minute
                        translatedText = translationMatch.TranslatedText;
                        break;

                    }

                    var o = new { Succeeded = true, Text = translatedText };

                    return MonoWorkaroundJson(o);
                }




                

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });
            }
        }

		[HttpGet]
		public ActionResult GetVersionText( string CorpusName, string VersionName )
		{
            try
            {
                IDocument versionDoc = PrismHelpers.GetVersion(CorpusName, VersionName);
                var o = new { Succeeded = true, Text = versionDoc.GetDocumentContent(0, versionDoc.Length(), false, true, null, false, null, null, null) };

                return MonoWorkaroundJson(o);
            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });
            }
		}

		[HttpGet]
		public ActionResult GetSegmentInformation(string CorpusName, string VersionName, bool includeAttribs)
		{
			try
			{
				IDocument doc = PrismHelpers.GetVersion(CorpusName, VersionName);
                var cbFilter = new List<SegmentContentBehaviour>() { SegmentContentBehaviour.normal };

				var segments = doc.FindSegmentDefinitions(0, doc.Length(), false, false, null, cbFilter.ToArray());
				
				var results = new List<SegmentDefinitionModel2>();
				foreach (var s in segments)
				{
					var result = new SegmentDefinitionModel2();
					result.ID = s.ID;
					result.length = s.Length;
					result.startPos = s.StartPosition;
					if (includeAttribs)
					{
						List<AttributeModel> attribs = new List<AttributeModel>();
						foreach (var a in s.Attributes)
						{
							var attrib = new AttributeModel();
							attrib.Name = a.Name;
							attrib.Value = a.Value;
							attribs.Add(attrib);
						}
						result.Attributes = attribs.ToArray();
					}
					results.Add(result);
				}
				
				var model = new SegmentDefinitionsModel();
				model.Succeeded = true;
				model.DocumentLength = doc.Length();
				model.SegmentDefinitions = results.ToArray();
				return MonoWorkaroundJson(model);
				
			}
			catch (Exception ex)
			{
				return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });
			}
		}

		[HttpGet]
		public ActionResult GetSegmentVariation( string CorpusName, int SegmentID, int? metricType )
		{
			try
			{
				ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);
				VariationMetricTypes temp = metricType.HasValue ? (VariationMetricTypes)(metricType) : VariationMetricTypes.metricA;

				var result = new SegmentVariationResultModel { Succeeded = true };
                System.Diagnostics.Debug.WriteLine("Getting variation data for segid: " + SegmentID.ToString());
				result.SegmentVariationData = corpus.CalculateSegmentVariation(SegmentID, temp, CorpusController.GetActiveCorpusSelection());
                System.Diagnostics.Debug.WriteLine("Done getting variation data for segid: " + SegmentID.ToString());

				return MonoWorkaroundJson(result);
			}
			catch (Exception ex)
			{
				return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });
			}
		}
	}

    [DataContract]
    public class AdmAccessToken
    {
        [DataMember]
        public string access_token { get; set; }
        [DataMember]
        public string token_type { get; set; }
        [DataMember]
        public string expires_in { get; set; }
        [DataMember]
        public string scope { get; set; }
    }

    public class AdmAuthentication
    {
        public static readonly string DatamarketAccessUri = "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13";
        private string clientId;
        private string clientSecret;
        private string request;
        private AdmAccessToken token;
        private Timer accessTokenRenewer;
        private object _lock = new object();

        //Access token expires every 10 minutes. Renew it every 9 minutes only.
        private const int RefreshTokenDuration = 9;

        private string EncodeString(string s)
        {
            return System.Uri.EscapeDataString(s);
        }


        public AdmAuthentication(string clientId, string clientSecret)
        {
            this.clientId = clientId;
            this.clientSecret = clientSecret;
            //If clientid or client secret has special characters, encode before sending request
            //string s = string.Empty;
            //s = "grant_type=client_credentials&client_id=" + HttpUtility.UrlEncode(clientId) + "&client_secret=" + HttpUtility.UrlEncode(clientSecret);// +"&scope=http://api.microsofttranslator.com";
            //this.request = s;
            this.request = string.Format("grant_type=client_credentials&client_id={0}&client_secret={1}&scope=http://api.microsofttranslator.com", EncodeString(clientId), EncodeString(clientSecret));
            this.token = HttpPost(DatamarketAccessUri, this.request);
            //renew the token every specfied minutes
            accessTokenRenewer = new Timer(new TimerCallback(OnTokenExpiredCallback), this, TimeSpan.FromMinutes(RefreshTokenDuration), TimeSpan.FromMilliseconds(-1));
        }

        public AdmAccessToken GetAccessToken()
        {
            lock (_lock)
                return this.token;
        }


        private void RenewAccessToken()
        {
            lock (_lock)
            {
                AdmAccessToken newAccessToken = HttpPost(DatamarketAccessUri, this.request);
                //swap the new token with old one

                this.token = newAccessToken;
            }
            Console.WriteLine(string.Format("Renewed token for user: {0} is: {1}", this.clientId, this.token.access_token));
        }

        private void OnTokenExpiredCallback(object stateInfo)
        {
            try
            {
                RenewAccessToken();
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Failed renewing access token. Details: {0}", ex.Message));
            }
            finally
            {
                try
                {
                    accessTokenRenewer.Change(TimeSpan.FromMinutes(RefreshTokenDuration), TimeSpan.FromMilliseconds(-1));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format("Failed to reschedule the timer to renew access token. Details: {0}", ex.Message));
                }
            }
        }


        private AdmAccessToken HttpPost(string DatamarketAccessUri, string requestDetails)
        {
            //Prepare OAuth request 
            WebRequest webRequest = WebRequest.Create(DatamarketAccessUri);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";
            byte[] bytes = Encoding.ASCII.GetBytes(requestDetails);
            webRequest.ContentLength = bytes.Length;
            using (Stream outputStream = webRequest.GetRequestStream())
            {
                outputStream.Write(bytes, 0, bytes.Length);
            }
            using (WebResponse webResponse = webRequest.GetResponse())
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(AdmAccessToken));
                //Get deserialized object from JSON stream
                AdmAccessToken token = (AdmAccessToken)serializer.ReadObject(webResponse.GetResponseStream());
                return token;
            }
        }
    }


}



