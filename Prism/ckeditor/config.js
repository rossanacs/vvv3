﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';

    config.toolbar = 'MyToolbar';

    config.removePlugins = 'scayt,menubutton,contextmenu';

    config.resize_enabled = false;
    config.toolbarCanCollapse = false;

    config.protectedSource = [/<q[\s\S]*?q>/g]; // see _segMarkerEditPlaceholder

    config.toolbar_MyToolbar =
	[
        { name: 'tb1', items: ['Source', '-', 'Find', '-', 'SelectAll'] },
        { name: 'tb1a', items: ['Edit', '-', 'Save', '-', 'Cancel'] },
        { name: 'tb3', items: ['ToggleMarkers', '-', 'ToggleColour'] },
        { name: 'tb4', items: ['InsertStartMarker', '-', 'InsertEndMarker'] },
		{ name: 'tb2', items: ['CreateSegment', '-', 'CreateSegmentFromMarkers'] },
        { name: 'tb5', items: ['EditSegment', '-', 'DeleteSegment', '-', 'DeleteAllSegments'] },
        { name: 'tb6', items: ['MoveToStartMarker', '-', 'MoveToEndMarker'] }
	];

    config.toolbar_Upload =
    [
        {name: 'tb1', items: ['Source']},
        { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
        { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt'] },
        { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
        { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv',
	        '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']
        },
        { name: 'colors', items: ['TextColor', 'BGColor'] }
    ];


};
